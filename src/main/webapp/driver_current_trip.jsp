<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="taxi.pojo.Trip" %>
<%@ page import="taxi.database.dao.TripDAOImpl" %>
<%@ page import="taxi.pojo.Trips" %>
<%@ page import="taxi.database.dao.TripDAO" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="java.io.*,java.util.*" %><%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 27.10.17
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Trip status</title>
</head>
<body>

<%
    // response.setIntHeader("Refresh", 1);
%>
    <%
        Trip trip = (Trip) request.getAttribute("trip");
   if(trip == null){
%>
    <h1>Free trips:
    <c:forEach items="${trips.getTrips()}" var="trip" varStatus="varStatus">
        <table>
            <td>Client: ${trip.getClient().getName()}</td>
            <td>, From: ${trip.getRideStartLocation()}</td>
            <td> To: ${trip.getRideEndLocation()}</td>
        </table>
        <br>
        <form id="accept" method="post" action="acceptTrip">
            <input type="hidden" name="currentTripId" value="${trip.getId()}">
            <input type="submit" value="accept trip">
        </form>
    </c:forEach>
    </h1>
    <form id="menu" method="get" action="/driverMenu">
        <input type="submit" value="Back to menu">
    </form>
<%
    }
    else {
        if(trip.getState().getStateName().equals("accepted")){
%>
    <h1>
        Your trip:<br>
        Client: <%= trip.getClient().getName()%><br>
        Payment method: <%= trip.getPaymentMethod()%><br>
        Trip start location: <%= trip.getRideStartLocation()%><br>
        Trip end location: <%= trip.getRideEndLocation()%><br>
        Trip start datetime: <%= trip.getRideStartTime()%><br>
        Trip status: <%= trip.getState().getStateName()%><br>
    </h1>
    <form id="prepare" method="post" action="/prepare">
        <input type="submit" value="prepare">
    </form>
    <form id="break" method="post" action="/breakTrip">
        <input type="submit" value="break">
    </form>
    <form id="menu" method="get" action="/driverMenu">
        <input type="submit" value="Back to menu">
    </form>
    <%
            }
        if(trip.getState().getStateName().equals("prepared")){
    %>
    <h1>
        Your trip:<br>
        Client: <%= trip.getClient().getName()%><br>
        Payment method: <%= trip.getPaymentMethod()%><br>
        Trip start location: <%= trip.getRideStartLocation()%><br>
        Trip end location: <%= trip.getRideEndLocation()%><br>
        Trip start datetime: <%= trip.getRideStartTime()%><br>
        Trip status: <%= trip.getState().getStateName()%><br>
    </h1>
    <form id="prepare" method="post" action="/start">
        <input type="submit" value="start">
    </form>
    <form id="menu" method="get" action="/driverMenu">
        <input type="submit" value="Back to menu">
    </form>
    <%
            }
        if(trip.getState().getStateName().equals("started")){
    %>
    <h1>
        Your trip:<br>
        Client: <%= trip.getClient().getName()%><br>
        Payment method: <%= trip.getPaymentMethod()%><br>
        Trip start location: <%= trip.getRideStartLocation()%><br>
        Trip end location: <%= trip.getRideEndLocation()%><br>
        Trip start datetime: <%= trip.getRideStartTime()%><br>
        Trip status: <%= trip.getState().getStateName()%><br>
    </h1>
    <form id="arrive" method="post" action="/arrive">
        <input type="submit" value="arrive">
    </form>
    <form id="menu" method="get" action="/driverMenu">
        <input type="submit" value="Back to menu">
    </form>
    <%
            }
        if(trip.getState().getStateName().equals("arrived")){
    %>
    <h1>
        Your trip:<br>
        Client: <%= trip.getClient().getName()%><br>
        Payment method: <%= trip.getPaymentMethod()%><br>
        Trip start location: <%= trip.getRideStartLocation()%><br>
        Trip end location: <%= trip.getRideEndLocation()%><br>
        Trip start datetime: <%= trip.getRideStartTime()%><br>
        Trip end datetime: <%= trip.getRideEndTime()%><br>
        Trip status: <%= trip.getState().getStateName()%><br>
        Waiting for payment..
    </h1>
    <form id="menu" method="get" action="/driverMenu">
        <input type="submit" value="Back to menu">
    </form>
    <%
            }
        if(trip.getState().getStateName().equals("paid")){
    %>
    <h1>
        Your trip:<br>
        Client: <%= trip.getClient().getName()%><br>
        Payment method: <%= trip.getPaymentMethod()%><br>
        Trip start location: <%= trip.getRideStartLocation()%><br>
        Trip end location: <%= trip.getRideEndLocation()%><br>
        Trip start datetime: <%= trip.getRideStartTime()%><br>
        Trip end datetime: <%= trip.getRideEndTime()%><br>
        Trip status: <%= trip.getState().getStateName()%><br>
        Trip successfully ended
    </h1>
    <form id="menu" method="get" action="/driverMenu">
        <input type="submit" value="Back to menu">
    </form>
    <form id="menu" method="get" action="/breakTrip">
        <input type="submit" value="Break trip">
    </form>
    <%
            }
        if(trip.getState().getStateName().equals("cancelled")){
    %>
    <h1>
        Your trip:<br>
        Client: <%= trip.getClient().getName()%><br>
        Payment method: <%= trip.getPaymentMethod()%><br>
        Trip start location: <%= trip.getRideStartLocation()%><br>
        Trip end location: <%= trip.getRideEndLocation()%><br>
        Trip start datetime: <%= trip.getRideStartTime()%><br>
        Trip status: <%= trip.getState().getStateName()%><br>
        Trip cancelled
    </h1>
    <form id="menu" method="get" action="/driverMenu">
        <input type="submit" value="Back to menu">
    </form>
    <form id="menu" method="get" action="/breakTrip">
        <input type="submit" value="Break trip">
    </form>
    <%
            }
    }
    %>
</body>
</html>
