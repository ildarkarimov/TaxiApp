<%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 25.10.17
  Time: 9:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Driver registration</title>
</head>
<body>
<h1>
    <%
        String answer = (String) request.getAttribute("answer");
        if (!(answer == null || answer.length() == 0)) {
    %>
    <%= answer%>
    <%
        }
    %>
</h1>
<form id="create" method="post" action="/driverRegistration">
    <jsp:text>name</jsp:text>
    <input type="text" name="name" value="${name}" required>
    <br>
    <jsp:text>card number</jsp:text>
    <input type="number" name="cardNumber" value="${cardNumber}" required>
    <br>
    <jsp:text>user name</jsp:text>
    <input type="text" name="userName" value="${userName}" size="30" minlength="8" required>
    <br>
    <jsp:text>password</jsp:text>
    <input type="password" name="password" value="${password}" size="30" minlength="8" required>
    <br>
    <jsp:text>Vehicle number:</jsp:text>
    <input type="text" name="number" value="${number}" size="30" required>
    <br>
    <jsp:text>Vehicle make:</jsp:text>
    <input type="text" name="make" value="${make}" size="30" required>
    <br>
    <jsp:text>Vehicle model:</jsp:text>
    <input type="text" name="model" value="${model}" size="30" required>
    <br>
    <jsp:text>Vehicle colour:</jsp:text>
    <input type="text" name="colour" value="${colour}" size="30" required>
    <input type="submit" value="create">
</form>
<form id="menu" method="get" action="/index.jsp">
    <input type="submit" value="Back to index">
</form>
</body>
</html>
