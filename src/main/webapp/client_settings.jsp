<%@ page import="taxi.database.dao.ClientDAO" %>
<%@ page import="taxi.pojo.Client" %>
<%@ page import="taxi.database.dao.ClientDAOImpl" %>
<%@ page import="taxi.database.exceptions.ClientGetException" %><%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 16.10.17
  Time: 23:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit client</title>
</head>
<body>
<h1>
    <%
        String answer = (String) request.getAttribute("answer");
        if (!(answer == null || answer.length() == 0)) {
    %>
    <%= answer%>
    <%
        }
    %>
</h1>

<% //Client client = (Client) ((HttpServletRequest)request).getSession().getAttribute("client");%>
<form id="edit" method="post" action="/clientEdit">
    <jsp:text>id</jsp:text>
    <input type="text" readonly name="id" value="${clientInfo.id}">
    <br>
    <jsp:text>name</jsp:text>
    <input type="text" name="name" value="${clientInfo.name}">
    <br>
    <jsp:text>card number</jsp:text>
    <input type="number" name="cardNumber" value="${clientInfo.cardNumber}">
    <br>
    <jsp:text>user name</jsp:text>
    <input type="text" readonly name="userName" value="${clientInfo.userName}">
    <br>
    <input type="submit" value="edit">
</form>
<form id="password" method="get" action="/clientEditPassword">
    <input type="submit" value="Edit password">
</form>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
<form id="logout" method="get" action="/clientLogout">
    <input type="submit" value="logout"/>
</form>
</body>
</html>
