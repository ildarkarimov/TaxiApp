<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="taxi.pojo.Trips" %>
<%@ page import="taxi.pojo.Client" %>
<%@ page import="java.util.List" %>
<%@ page import="taxi.pojo.Trip" %>
<%@ page import="org.springframework.web.bind.annotation.SessionAttributes" %><%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 15.10.17
  Time: 21:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Our trips</title>
</head>
<body>
<h1>
    Our trips:
    <br>
</h1>
<c:forEach items="${clientTripsList.getTrips()}" var="trip" varStatus="varStatus">
    <table>
        <td>Driver: ${trip.getDriver().getName()}</td>
        <td>, Vehicle: ${trip.getDriver().getNumber()}</td>
        <td>, Make: ${trip.getDriver().getMake()}</td>
        <td>, Model: ${trip.getDriver().getModel()}</td>
        <td>, Cost: ${trip.getCost()}</td>
        <td>, From: ${trip.getRideStartLocation()}</td>
        <td> To: ${trip.getRideEndLocation()}</td>
        <td>, Date: ${trip.getRideEndTime()}</td>
    </table>
</c:forEach>

<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
<form id="logout" method="get" action="/clientLogout">
    <input type="submit" value="logout"/>
</form>
</body>
</html>
