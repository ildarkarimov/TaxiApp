<%@ page import="taxi.pojo.Trip" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="java.io.*,java.util.*" %><%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 26.10.17
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Trip status</title>
</head>
<body>

    <%
       // response.setIntHeader("Refresh", 1);
    %>

<%
    Trip trip = (Trip) request.getAttribute("trip");
    if(trip == null){
%>

<h1>You dont' have trips</h1>
<form id="request" method="get" action="/client_trip_form.jsp">
    <input type="submit" value="request trip">
</form>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
<%
    }
    if (!(trip == null)) {

    if(trip.getState().getStateName().equals("requested")){
        %>
<h1>
    Your trip:<br>
    Payment method: <%= trip.getPaymentMethod()%><br>
    Trip start location: <%= trip.getRideStartLocation()%><br>
    Trip end location: <%= trip.getRideEndLocation()%><br>
    Trip status: <%= trip.getState().getStateName()%><br>
    Searching for driver..
</h1>
<form id="cancel" method="post" action="/cancel">
    <input type="submit" value="cancel">
</form>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
        <%
    }
    if(trip.getState().getStateName().equals("accepted")){
        %>
<h1>
    Your trip:<br>
    Payment method: <%= trip.getPaymentMethod()%><br>
    Trip start location: <%= trip.getRideStartLocation()%><br>
    Trip end location: <%= trip.getRideEndLocation()%><br>
    Trip status: <%= trip.getState().getStateName()%><br>
    Driver: <%= trip.getDriver().getName()%><br>
    Vehicle number: <%= trip.getDriver().getName()%><br>
    Vehicle make: <%= trip.getDriver().getMake()%><br>
    Vehicle model: <%= trip.getDriver().getModel()%><br>
    Vehicle colour: <%= trip.getDriver().getColour()%><br>
    Waiting for driver..
</h1>
<form id="cancel" method="post" action="/cancel">
    <input type="submit" value="cancel">
</form>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
        <%
    }
    if(trip.getState().getStateName().equals("prepared")){
        %>
<h1>
Your trip:<br>
Payment method: <%= trip.getPaymentMethod()%><br>
Trip start location: <%= trip.getRideStartLocation()%><br>
Trip end location: <%= trip.getRideEndLocation()%><br>
Trip status: <%= trip.getState().getStateName()%><br>
Driver: <%= trip.getDriver().getName()%><br>
Vehicle number: <%= trip.getDriver().getName()%><br>
Vehicle make: <%= trip.getDriver().getMake()%><br>
Vehicle model: <%= trip.getDriver().getModel()%><br>
Vehicle colour: <%= trip.getDriver().getColour()%><br>
Vehicle waiting for you</h1>
<form id="cancel" method="post" action="/cancel">
    <input type="submit" value="cancel">
</form>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
        <%
            }
        if(trip.getState().getStateName().equals("started")){
        %>
<h1>
Your trip:<br>
Payment method: <%= trip.getPaymentMethod()%><br>
Trip start location: <%= trip.getRideStartLocation()%><br>
Trip end location: <%= trip.getRideEndLocation()%><br>
Trip status: <%= trip.getState().getStateName()%><br>
Driver: <%= trip.getDriver().getName()%><br>
Vehicle number: <%= trip.getDriver().getName()%><br>
Vehicle make: <%= trip.getDriver().getMake()%><br>
Vehicle model: <%= trip.getDriver().getModel()%><br>
Vehicle colour: <%= trip.getDriver().getColour()%><br>
Trip in process</h1>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
<%
        }
    if(trip.getState().getStateName().equals("arrived")){
%>
<h1>
Your trip:<br>
Payment method: <%= trip.getPaymentMethod()%><br>
Trip start location: <%= trip.getRideStartLocation()%><br>
Trip end location: <%= trip.getRideEndLocation()%><br>
Trip status: <%= trip.getState().getStateName()%><br>
Driver: <%= trip.getDriver().getName()%><br>
Vehicle number: <%= trip.getDriver().getName()%><br>
Vehicle make: <%= trip.getDriver().getMake()%><br>
Vehicle model: <%= trip.getDriver().getModel()%><br>
Vehicle colour: <%= trip.getDriver().getColour()%><br>
</h1>
<form id="request" method="post" action="/pay">
    <input type="submit" value="pay">
</form>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
<%
    }
    if(trip.getState().getStateName().equals("paid")){
%>
<h1>
Your trip:<br>
Payment method: <%= trip.getPaymentMethod()%><br>
Trip start location: <%= trip.getRideStartLocation()%><br>
Trip end location: <%= trip.getRideEndLocation()%><br>
Trip status: <%= trip.getState().getStateName()%><br>
Driver: <%= trip.getDriver().getName()%><br>
Vehicle number: <%= trip.getDriver().getName()%><br>
Vehicle make: <%= trip.getDriver().getMake()%><br>
Vehicle model: <%= trip.getDriver().getModel()%><br>
Vehicle colour: <%= trip.getDriver().getColour()%><br>
Trip successfully ended</h1>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
<form id="request" method="get" action="/client_trip_form.jsp">
    <input type="submit" value="request new trip">
</form>
<%
        }
    if(trip.getState().getStateName().equals("cancelled")){
%>
<h1>
    Your trip:<br>
    Payment method: <%= trip.getPaymentMethod()%><br>
    Trip start location: <%= trip.getRideStartLocation()%><br>
    Trip end location: <%= trip.getRideEndLocation()%><br>
    Trip status: <%= trip.getState().getStateName()%><br>
    Trip canceled
</h1>
<form id="request" method="get" action="/client_trip_form.jsp">
    <input type="submit" value="request new trip">
</form>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
<%
        }


            }
%>
</body>
</html>