<%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 15.10.17
  Time: 9:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Starting page</title>
  </head>
  <body>
  <h1>Client login:</h1>
  <h2>
    <%
      String answerClient = (String) request.getAttribute("answerClient");
      if (!(answerClient == null || answerClient.length() == 0)) {
    %>
    <%= answerClient%>
    <%
      }
    %>
  </h2>
  <form id="client" method="post" action="/authClient">
    <jsp:text>user Name</jsp:text>
    <input type="text" name="userName" size="30" minlength="8" required/>
    <br>
    <jsp:text>password</jsp:text>
    <input type="password" name="password" size="30" minlength="8" required/>
    <br>
    <input type="submit" value="login" >
  </form>
  <form id="newClient" method="get" action="/clientRegistration">
    <input type="submit" value="Client Registration"/>
  </form>
  <h1>Driver login:</h1>
  <h2>
    <%
      String answerDriver = (String) request.getAttribute("answerDriver");
      if (!(answerDriver == null || answerDriver.length() == 0)) {
    %>
    <%= answerDriver%>
    <%
      }
    %>
  </h2>
  <form id="driver" method="post" action="/authDriver">
    <jsp:text>Driver name</jsp:text>
    <input type="text" size="30" minlength="8" required name="userName"/>
    <br>
    <jsp:text>password</jsp:text>
    <input type="password" size="30" minlength="8" required name="password"/>
    <br>
    <input type="submit" value="login">
  </form>
  <form id="newDriver" method="get" action="/driverRegistration">
    <input type="submit" value="Driver Registration"/>
  </form>
  </body>
</html>
