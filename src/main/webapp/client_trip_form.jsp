<%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 27.10.17
  Time: 19:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New Trip</title>
</head>
<body>
<form id="create" method="post" action="/requestTrip">
    <jsp:text>Trip start location</jsp:text>
    <input type="text" size="50" minlength="10" name="startLocation"required>
    <br>
    <jsp:text>Trip end location</jsp:text>
    <input type="text" size="50" minlength="10" name="endLocation" required>
    <br>
    <jsp:text>Payment method</jsp:text>
    <input type="text" size="50" minlength="4" name="paymentMethod" required>
    <br>
    <input type="submit" value="request">
</form>
</body>
</html>
