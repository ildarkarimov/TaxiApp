<%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 18.10.17
  Time: 7:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit password</title>
</head>
<body>
<h1>
    <%
        String answer = (String) request.getAttribute("answer");
        if (!(answer == null || answer.length() == 0)) {
    %>
    <%= answer%>
    <%
        }
    %>
</h1>
<form id="edit" method="post" action="/clientEditPassword">
    <jsp:text>old password</jsp:text>
    <input type="password" size="30" minlength="8" required name="oldPassword" value="${oldPassword}">
    <br>
    <jsp:text>new password</jsp:text>
    <input type="password" size="30" minlength="8" required name="newPassword" value="${newPassword}">
    <br>
    <input type="submit" value="edit">
</form>
<form id="account" method="post" action="/clientSettings">
    <input type="submit" value="back to account">
</form>
<form id="menu" method="get" action="/clientMenu">
    <input type="submit" value="Back to menu">
</form>
<form id="logout" method="get" action="/clientLogout">
    <input type="submit" value="logout"/>
</form>
</body>
</html>
