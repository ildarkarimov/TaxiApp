<%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 22.10.17
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Driver menu</title>
</head>
<body>
<form id="currentTrips" method="get" action="/TripCurrentDriver">
    <input type="submit" value="current trip">
</form>
<form id="trips" method="post" action="/driverTrips">
    <input type="submit" value="trips">
</form>
<form id="account" method="post" action="/driverSettings">
    <input type="submit" value="account settings"/>
</form>
<form id="logout" method="get" action="/driverLogout">
    <input type="submit" value="logout"/>
</form>
</body>
</html>
