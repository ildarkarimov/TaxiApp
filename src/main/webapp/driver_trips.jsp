<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 26.10.17
  Time: 0:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Trips</title>
</head>
<body>
<h1>
    Our trips:
    <br>
</h1>
<c:forEach items="${driverTripsList.getTrips()}" var="trip" varStatus="varStatus">
    <table>
        <td>Client Name: ${trip.getClient().getName()}</td>
        <td>,Client UserName: ${trip.getClient().getUserName()}</td>
        <td>, Cost: ${trip.getCost()}</td>
        <td>, From: ${trip.getRideStartLocation()}</td>
        <td> To: ${trip.getRideEndLocation()}</td>
        <td>, Date: ${trip.getRideEndTime()}</td>
    </table>
</c:forEach>

<form id="menu" method="get" action="/driverMenu">
    <input type="submit" value="Back to menu">
</form>
<form id="logout" method="get" action="/driverLogout">
    <input type="submit" value="logout"/>
</form>
</body>
</html>
