<%--
  Created by IntelliJ IDEA.
  User: ildar
  Date: 25.10.17
  Time: 23:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Driver settings</title>
</head>
<body>
<h1>
    <%
        String answer = (String) request.getAttribute("answer");
        if (!(answer == null || answer.length() == 0)) {
    %>
    <%= answer%>
    <%
        }
    %>
</h1>

<form id="edit" method="post" action="/driverEdit">
    <jsp:text>id</jsp:text>
    <input type="text" readonly name="id" value="${driverInfo.id}">
    <br>
    <jsp:text>name</jsp:text>
    <input type="text" name="name" value="${driverInfo.name}">
    <br>
    <jsp:text>card number</jsp:text>
    <input type="number" name="cardNumber" value="${driverInfo.cardNumber}">
    <br>
    <jsp:text>user name</jsp:text>
    <input type="text" readonly name="userName" value="${driverInfo.userName}">
    <br>
    <jsp:text>vehicle number</jsp:text>
    <input type="text" name="number" value="${driverInfo.number}">
    <br>
    <jsp:text>vehicle make</jsp:text>
    <input type="text" name="make" value="${driverInfo.make}">
    <br>
    <jsp:text>vehicle model</jsp:text>
    <input type="text" name="model" value="${driverInfo.model}">
    <br>
    <jsp:text>vehicle colour</jsp:text>
    <input type="text" name="colour" value="${driverInfo.colour}">
    <br>
    <input type="submit" value="edit">
</form>
<form id="password" method="get" action="/driverEditPassword">
    <input type="submit" value="Edit password">
</form>
<form id="menu" method="get" action="/driverMenu">
    <input type="submit" value="Back to menu">
</form>
<form id="logout" method="get" action="/driverLogout">
    <input type="submit" value="logout"/>
</form>
</body>
</html>
