package taxi.servlets.trip;

import taxi.database.exceptions.TripGetException;
import org.apache.log4j.Logger;
import taxi.pojo.DriverInfo;
import taxi.pojo.Trips;
import taxi.services.trip.DriverTripService;
import taxi.services.trip.DriverTripServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DriverTripServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ClientTripServlet.class);
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DriverTripService driverTripService = new DriverTripServiceImpl();
        Trips driverTripsList = null;
        int id = ((DriverInfo) req.getSession().getAttribute("driverInfo")).getId();
        driverTripsList = driverTripService.showDriverTrips(id);
        req.setAttribute("driverTripsList", driverTripsList);
        req.getRequestDispatcher("driver_trip.jsp").forward(req, resp);
    }
}
