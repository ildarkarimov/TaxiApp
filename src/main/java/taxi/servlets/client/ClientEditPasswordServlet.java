package taxi.servlets.client;

import taxi.database.exceptions.ClientInsertException;
import org.apache.log4j.Logger;
import taxi.pojo.ClientInfo;
import taxi.services.client.ClientEditService;
import taxi.services.client.ClientEditServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientEditPasswordServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ClientRegistrationServlet.class);
   /* @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String answer = null;
        int id = ((ClientInfo) req.getSession().getAttribute("clientInfo")).getId();
        String oldPassword = req.getParameter("oldPassword");
        String newPassword = req.getParameter("newPassword");
        ClientEditService clientEditService = new ClientEditServiceImpl();
        try {
            answer = clientEditService.editPassword(id, oldPassword, newPassword);
        } catch (ClientInsertException e) {
            logger.error(e.getMessage());
        }
        req.setAttribute("answer", answer);
        req.setAttribute("oldPassword", oldPassword);
        req.setAttribute("newPassword", newPassword);
        getServletContext().getRequestDispatcher("/client_edit_password.jsp").forward(req, resp);

    }*/

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //resp.sendRedirect("/client_edit_password.jsp");
        req.getRequestDispatcher("/client_edit_password.jsp").forward(req,resp);    }

}
