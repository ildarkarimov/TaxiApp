package taxi.servlets.client;

import taxi.database.exceptions.ClientGetException;
import org.apache.log4j.Logger;
import taxi.services.client.ClientRegistrationService;
import taxi.services.client.ClientRegistrationServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientRegistrationServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ClientRegistrationServlet.class);
    String answer;
   /* @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        int cardNumber = 0;
        if(!req.getParameter("cardNumber").equals(""))
            cardNumber = Integer.valueOf(req.getParameter("cardNumber"));
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        ClientRegistrationService registrationService = new ClientRegistrationServiceImpl();
        try {
            answer = registrationService.regClient(name, cardNumber, userName, password);
        } catch (ClientGetException e) {
            logger.error(e.getMessage());
        }
        req.setAttribute("answer", answer);
        req.setAttribute("name", name);
        req.setAttribute("cardNumber", cardNumber);
        req.setAttribute("userName", userName);
        req.setAttribute("password", password);
            getServletContext().getRequestDispatcher("/client_registration.jsp").forward(req, resp);

    }*/

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //req.getRequestDispatcher("/client_registration.jsp").forward(req,resp);
        resp.sendRedirect("/client_registration.jsp");
    }
}
