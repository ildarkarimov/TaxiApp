package taxi.servlets.client;

import taxi.database.exceptions.ClientInsertException;
import org.apache.log4j.Logger;
import taxi.pojo.ClientInfo;
import taxi.services.client.ClientEditService;
import taxi.services.client.ClientEditServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientEditServlet extends HttpServlet{
    private static final Logger logger = Logger.getLogger(ClientRegistrationServlet.class);
    private String answer;
  /*  @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = ((ClientInfo) req.getSession().getAttribute("clientInfo")).getId();
        String name = req.getParameter("name");
        int cardNumber = Integer.valueOf(req.getParameter("cardNumber"));
        ClientEditService clientEditService = new ClientEditServiceImpl();
        try {
            answer = clientEditService.editClient(id, name, cardNumber);
        } catch (ClientInsertException e) {
            logger.error(e.getMessage());
        }
        if(answer.equals("Update is succeeded")){
            ((ClientInfo) req.getSession().getAttribute("clientInfo")).setCardNumber(cardNumber);
            ((ClientInfo) req.getSession().getAttribute("clientInfo")).setName(name);
        }
        req.setAttribute("answer", answer);
        getServletContext().getRequestDispatcher("/client_settings.jsp").forward(req, resp);
    }*/

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //req.getRequestDispatcher("/client_settings.jsp").forward(req,resp);
        resp.sendRedirect("/client_settings.jsp");
    }
}
