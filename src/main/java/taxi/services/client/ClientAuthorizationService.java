package taxi.services.client;

import taxi.pojo.ClientInfo;

public interface ClientAuthorizationService {
    ClientInfo authClient(String login, String password);
}
