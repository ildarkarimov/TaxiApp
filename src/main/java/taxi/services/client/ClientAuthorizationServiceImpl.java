package taxi.services.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import taxi.database.dao.ClientDAO;
import taxi.database.dao.ClientDAOImpl;
import taxi.database.exceptions.ClientGetException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import taxi.pojo.Client;
import taxi.pojo.ClientInfo;

@Component
public class ClientAuthorizationServiceImpl implements ClientAuthorizationService {
    private static final Logger logger = Logger.getLogger(ClientAuthorizationServiceImpl.class);
  @Autowired
  ClientInfo clientInfo;
  @Autowired
  ClientDAO clientDAO;


    @Override
    public ClientInfo authClient(String login, String password) {

        try {
            Client client;
            if((client = clientDAO.getClientByNameAndPassword(login, password)) != null) {
                clientInfo.setId(client.getId());
                clientInfo.setCardNumber(client.getCardNumber());
                clientInfo.setName(client.getName());
                clientInfo.setUserName(client.getUserName());
                return clientInfo;
            }
        } catch (ClientGetException e) {
            logger.error(e.getMessage());
        }
        return null;

    }
}
