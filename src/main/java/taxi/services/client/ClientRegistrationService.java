package taxi.services.client;

import taxi.database.exceptions.ClientGetException;
import taxi.pojo.Client;

public interface ClientRegistrationService {
    String regClient(String name, Integer cardNumber, String userName, String password);
    boolean isRegExpIncorrect(String login);
    boolean isClientRegistrationSucceed(Client client);
}
