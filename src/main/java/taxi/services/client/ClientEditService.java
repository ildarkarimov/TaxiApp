package taxi.services.client;

import org.springframework.stereotype.Component;
import taxi.database.exceptions.ClientInsertException;
@Component
public interface ClientEditService {
    public boolean editClient(int id, String name, int cardNumber);
    public String editPassword(int id, String oldPassword, String newPassword);
}
