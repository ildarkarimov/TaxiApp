package taxi.services.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.dao.ClientDAO;
import taxi.database.dao.ClientDAOImpl;
import taxi.database.exceptions.ClientGetException;
import taxi.database.exceptions.ClientInsertException;
import org.apache.log4j.Logger;
import taxi.pojo.Client;
import taxi.services.PasswordEncoder;
@Component
public class ClientEditServiceImpl implements ClientEditService {
    private static final Logger logger = Logger.getLogger(ClientEditServiceImpl.class);
    //ClientDAO clientDAO = new ClientDAOImpl();
    @Autowired
    ClientDAO clientDAO;

    @Override
    public boolean editClient(int id, String name, int cardNumber){
        try {
            if (clientDAO.updateClient(name, cardNumber, id))
                    return true;
        } catch (ClientInsertException e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    public String editPassword(int id, String oldPassword, String newPassword){
            String password = null;
            password = clientDAO.getClientById(id).getPassword();

            if (!((PasswordEncoder.encode(oldPassword).equals(password)))) {
                return "Incorrect old password";
            }

        try {
            if(!clientDAO.updatePassword(id, PasswordEncoder.encode(newPassword)))
                        return "Error on the servet. Please try one more time";
        } catch (ClientInsertException e) {
            logger.error(e.getMessage());
        }
        return "Password updated successfully";
        }


}
