package taxi.services.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.dao.ClientDAO;
import taxi.database.dao.ClientDAOImpl;
import taxi.database.exceptions.ClientGetException;
import taxi.database.exceptions.ClientInsertException;
import org.apache.log4j.Logger;
import taxi.pojo.Client;
import taxi.pojo.User;
import taxi.services.PasswordEncoder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
@Component
public class ClientRegistrationServiceImpl implements ClientRegistrationService {
    private static final Logger logger = Logger.getLogger(ClientRegistrationServiceImpl.class);
    //private static ClientDAO clientDAO = new ClientDAOImpl();
    @Autowired
    ClientDAO clientDAO;
    public String answer;

    @Override
    public boolean isClientRegistrationSucceed(Client client) {
        try {
            if(!clientDAO.insertClient(client))
                return false;
        } catch (ClientInsertException e) {
            logger.error(e.getMessage());
        }
        return true;
    }


    @Override
    public String regClient(String name, Integer cardNumber, String userName, String password) {
       if (isRegExpIncorrect(userName))
           return "Illegal sign in login";
       else try {
           if(clientDAO.isUserNameExist(userName))
               return "UserName already exist";
               else if(!isClientRegistrationSucceed(new Client(
                       name, cardNumber, userName, PasswordEncoder.encode(password))))
                   return "Registration failure. We are sorry, try one more time";
       } catch (ClientGetException e) {
           logger.error(e.getMessage());
       }
        return "Registration is succeded";
    }

    @Override
    public boolean isRegExpIncorrect(String login){
        Pattern p = Pattern.compile("\\W");
        Matcher m = p.matcher(login);
        while(m.find())
            return true;
        return false;
    }
}
