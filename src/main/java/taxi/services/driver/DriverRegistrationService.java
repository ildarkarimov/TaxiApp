package taxi.services.driver;

import taxi.pojo.Driver;

public interface DriverRegistrationService {
    public boolean isDriverRegistrationSucceed(Driver driver);
    public String regDriver(String name, Integer cardNumber, String userName, String password,
                            String number, String make, String model, String colour);
}
