package taxi.services.driver;

import org.springframework.stereotype.Component;

@Component
public interface DriverEditService {
    public String editPassword(int id, String oldPassword, String newPassword);
    public boolean editDriver(int id, String name, int cardNumber, String number, String make,
                              String model, String colour);
}
