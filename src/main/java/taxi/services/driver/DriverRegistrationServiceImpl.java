package taxi.services.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.dao.ClientDAO;
import taxi.database.dao.DriverDAO;
import taxi.database.exceptions.ClientGetException;
import taxi.database.exceptions.DriverGetException;
import taxi.database.exceptions.DriverInsertException;
import taxi.pojo.Driver;
import taxi.services.PasswordEncoder;
import taxi.services.client.ClientRegistrationService;
import taxi.services.client.ClientRegistrationServiceImpl;
import java.util.logging.Logger;
@Component
public class DriverRegistrationServiceImpl implements DriverRegistrationService {
    private static final org.apache.log4j.Logger logger =
            org.apache.log4j.Logger.getLogger(DriverRegistrationService.class);
    //private static ClientDAO clientDAO = new ClientDAOImpl();
    @Autowired
    DriverDAO driverDAO;
    @Autowired
    ClientRegistrationService clientRegistrationService;
    public String answer;

    @Override
    public boolean isDriverRegistrationSucceed(Driver driver) {
            if(!driverDAO.insertDriver(driver))
                return false;
        return true;
    }
    @Override
    public String regDriver(String name, Integer cardNumber, String userName, String password,
                            String number, String make, String model, String colour) {
        if (clientRegistrationService.isRegExpIncorrect(userName))
            return "Illegal sign in login";
        else if(driverDAO.isUserNameExist(userName))
            return "UserName already exist";
        else if(!isDriverRegistrationSucceed(new Driver(
                        name, cardNumber, userName, PasswordEncoder.encode(password),
                        number, make, model, colour)))
            return "Registration failure. We are sorry, try one more time";
        return "Registration is succeded";
    }
}
