package taxi.services.driver;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.dao.ClientDAO;
import taxi.database.dao.DriverDAO;
import taxi.database.exceptions.ClientInsertException;
import taxi.database.exceptions.DriverInsertException;
import taxi.services.PasswordEncoder;
import taxi.services.client.ClientEditServiceImpl;

@Component
public class DriverEditServiceImpl implements DriverEditService {
    private static final Logger logger = Logger.getLogger(DriverEditServiceImpl.class);
    @Autowired
    DriverDAO driverDAO;

    @Override
    public boolean editDriver(int id, String name, int cardNumber, String number, String make,
                              String model, String colour){
        if (driverDAO.updateDriver(name, cardNumber, number, make, model, colour, id))
            return true;
        return false;
    }
    public String editPassword(int id, String oldPassword, String newPassword){
        String password = driverDAO.getDriverById(id).getPassword();
        if (!((PasswordEncoder.encode(oldPassword).equals(password))))
            return "Incorrect old password";

        if(!driverDAO.updatePassword(id, PasswordEncoder.encode(newPassword)))
            return "Error on the server. Please try one more time";
        return "Password updated successfully";
    }

}
