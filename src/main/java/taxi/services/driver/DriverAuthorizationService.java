package taxi.services.driver;

import org.springframework.stereotype.Component;
import taxi.pojo.Client;
import taxi.pojo.Driver;
import taxi.pojo.DriverInfo;

public interface DriverAuthorizationService {
    DriverInfo authDriver(String login, String password);
}
