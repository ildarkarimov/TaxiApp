package taxi.services.driver;

import org.springframework.beans.factory.annotation.Autowired;
import taxi.database.dao.DriverDAO;
import taxi.database.dao.DriverDAOImpl;
import taxi.database.exceptions.DriverGetException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import taxi.pojo.Driver;
import taxi.pojo.DriverInfo;

@Component
public class DriverAuthorizationServiceImpl implements DriverAuthorizationService {
    private static final Logger logger = Logger.getLogger(DriverAuthorizationServiceImpl.class);
    @Autowired
    DriverDAO driverDAO;
    @Autowired
    DriverInfo driverInfo;
    //private static DriverDAO driverDAO = new DriverDAOImpl();

    @Override
    public DriverInfo authDriver(String login, String password) {
            Driver driver;
            if((driver = driverDAO.getDriverByNameAndPassword(login, password)) != null) {
                driverInfo.setId(driver.getId());
                driverInfo.setName(driver.getName());
                driverInfo.setCardNumber(driver.getCardNumber());
                driverInfo.setUserName(driver.getUserName());
                driverInfo.setNumber(driver.getNumber());
                driverInfo.setMake(driver.getMake());
                driverInfo.setModel(driver.getModel());
                driverInfo.setColour(driver.getColour());
                return driverInfo;
            }
        return null;
    }
}
