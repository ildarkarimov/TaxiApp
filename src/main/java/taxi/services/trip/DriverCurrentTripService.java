package taxi.services.trip;

import org.springframework.stereotype.Component;
import taxi.pojo.Trip;

@Component
public interface DriverCurrentTripService {
    public Trip acceptTrip(int driverId, int tripId);
    public Trip setState(int tripId, String state);
}
