package taxi.services.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.dao.ClientDAO;
import taxi.database.dao.TripDAO;
import taxi.pojo.Client;
import taxi.pojo.ClientInfo;
import taxi.pojo.Trip;
import taxi.pojo.state.CancelledState;
import taxi.pojo.state.InitState;
import taxi.pojo.state.RequestedState;
import taxi.pojo.state.State;

@Component
public class ClientCurrentTripServiceImpl implements ClientCurrentTripService {
    @Autowired
    Trip trip;
    @Autowired
    ClientDAO clientDAO;
    @Autowired
    TripDAO tripDAO;

    @Override
    public Trip requestClientTrip(int clientId, String startLocation, String endLocation, String paymentMethod) {
        Client client = clientDAO.getClientById(clientId);
        int tripId = tripDAO.getTripsCount() + 1;
        System.out.println("tripId" + tripId);
        trip.setId(tripId);
        trip.setClient(client);
        trip.setState(new RequestedState(trip));
        trip.setRideStartLocation(startLocation);
        trip.setRideEndLocation(endLocation);
        trip.setPaymentMethod(paymentMethod);
        tripDAO.insertTripRequest(trip);

        return trip;
    }
    @Override
    public Trip cancelTrip(int tripId) {
        trip = tripDAO.getTripById(tripId);
        trip.setState(new CancelledState(trip));
        tripDAO.setState(tripId, "cancelled");
        return trip;
    }
}
