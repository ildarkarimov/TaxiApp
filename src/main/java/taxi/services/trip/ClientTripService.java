package taxi.services.trip;

import org.springframework.stereotype.Component;
import taxi.database.exceptions.TripGetException;
import taxi.pojo.Trips;
@Component
public interface ClientTripService {
    Trips showClientTrips(int clientId);
}
