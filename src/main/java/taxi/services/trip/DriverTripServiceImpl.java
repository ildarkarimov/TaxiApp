package taxi.services.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.dao.TripDAO;
import taxi.database.dao.TripDAOImpl;
import taxi.database.exceptions.TripGetException;
import taxi.pojo.Trips;
@Component
public class DriverTripServiceImpl implements DriverTripService {
    @Autowired
    TripDAO tripDAO;
    //TripDAO tripDAO = new TripDAOImpl();
    @Override
    public Trips showDriverTrips(int driverId){
        Trips driverTrips = null;
        driverTrips = tripDAO.getDriverTrips(driverId);
        if(driverTrips==null)
            return null;
        return driverTrips;
    }
}
