package taxi.services.trip;

import org.springframework.stereotype.Component;
import taxi.pojo.ClientInfo;
import taxi.pojo.Trip;
import taxi.pojo.Trips;
@Component
public interface ClientCurrentTripService {
    public Trip requestClientTrip(int clientId, String startLocation, String endLocation, String paymentMethod);
    public Trip cancelTrip(int tripId);
}
