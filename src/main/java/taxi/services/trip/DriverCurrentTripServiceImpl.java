package taxi.services.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.dao.DriverDAO;
import taxi.database.dao.TripDAO;
import taxi.pojo.Driver;
import taxi.pojo.Trip;
import taxi.pojo.state.AcceptedState;
import taxi.pojo.state.InitState;
import taxi.pojo.state.PreparedState;
import taxi.pojo.state.State;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
@Component
public class DriverCurrentTripServiceImpl implements DriverCurrentTripService {
    @Autowired
    Trip trip;
    @Autowired
    DriverDAO driverDAO;
    @Autowired
    TripDAO tripDAO;

    @Override
    public Trip acceptTrip(int driverId, int tripId) {
        Random rn = new Random();
        int maximum = 1000;
        int minimum = 100;
        int n = maximum - minimum + 1;
        int i = rn.nextInt() % n;
        int randomNum =  minimum + i;
        Double cost = (double) randomNum;
        /*Date dt = new Date();
        String date = dt.toString();*/
        Driver driver = driverDAO.getDriverById(driverId);
        Trip trip = tripDAO.getTripById(tripId);
        trip.setCost(cost);
        trip.setId(tripId);
        trip.setDriver(driver);
        /*trip.setRideStartTime(date);*/
        //State state = trip.getState();
        //state.accept();
        trip.setState(new AcceptedState(trip));
        tripDAO.updateTripAccept(trip);

        return trip;
    }
    @Override
    public Trip setState(int tripId, String state){
        Trip trip = tripDAO.getTripById(tripId);
        tripDAO.setState(tripId, state);
        return trip;
    }
}
