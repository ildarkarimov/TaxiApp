package taxi.services.trip;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import taxi.database.dao.TripDAO;
import taxi.database.dao.TripDAOImpl;
import taxi.database.exceptions.TripGetException;
import taxi.pojo.Trips;
@Controller
public class ClientTripServiceImpl implements ClientTripService {
    private static final Logger logger = Logger.getLogger(ClientTripServiceImpl.class);
    //TripDAO tripDAO = new TripDAOImpl();
    @Autowired
    TripDAO tripDAO;
    @Override
    public Trips showClientTrips(int clientId) {
        Trips clientTrips = tripDAO.getClientTrips(clientId);
        if(clientTrips==null)
            return null;
        return clientTrips;
    }
}
