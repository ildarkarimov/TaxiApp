package taxi.controllers.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import taxi.database.dao.TripDAO;
import taxi.pojo.DriverInfo;
import taxi.pojo.Trip;
import taxi.pojo.Trips;
import taxi.services.trip.ClientCurrentTripServiceImpl;
import taxi.services.trip.DriverCurrentTripService;

@Controller
@SessionAttributes({"driverInfo", "currentTripId"})
public class CurrentTripAcceptController {
    @Autowired
    DriverCurrentTripService driverCurrentTripService;
    @Autowired
    TripDAO tripDAO;

    @RequestMapping(value = "/acceptTrip", method = RequestMethod.GET)
    public ModelAndView showTripProgress(@ModelAttribute(value = "currentTripId") Integer tripId){
        ModelAndView modelAndView = new ModelAndView();
        Trip trip;
        if(tripId != 0) {
            trip = tripDAO.getTripById(tripId);
            modelAndView.addObject("trip", trip);
        }
        else {
            Trips trips = tripDAO.getAllFreeTrips();
            modelAndView.addObject("trips", trips);
        }
        modelAndView.setViewName("driver_current_trip");
        return modelAndView;
    }


    @RequestMapping(value = "/acceptTrip", method = RequestMethod.POST)
    ModelAndView acceptTrip(@ModelAttribute DriverInfo driverInfo,
                             @RequestParam(name = "currentTripId", required = true) int tripId) {
        Trip trip = driverCurrentTripService.acceptTrip(driverInfo.getId(), tripId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("currentTripId", tripId);
        modelAndView.addObject("trip", trip);
        modelAndView.setViewName("driver_current_trip");
        return modelAndView;
    }
}