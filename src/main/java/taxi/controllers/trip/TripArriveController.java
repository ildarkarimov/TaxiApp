package taxi.controllers.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import taxi.database.dao.TripDAO;
import taxi.pojo.Trip;
import taxi.pojo.Trips;
import taxi.services.trip.DriverCurrentTripService;

@Controller
@SessionAttributes(value = "currentTripId")
public class TripArriveController {
    @Autowired
    DriverCurrentTripService driverCurrentTripService;
    @Autowired
    TripDAO tripDAO;

    @RequestMapping(value = "/arrive", method = RequestMethod.GET)
    public ModelAndView showTripProgress(@ModelAttribute(value = "currentTripId") Integer tripId){
        ModelAndView modelAndView = new ModelAndView();
        Trip trip;
        if(tripId != 0) {
            trip = tripDAO.getTripById(tripId);
            modelAndView.addObject("trip", trip);
        }
        else {
            Trips trips = tripDAO.getAllFreeTrips();
            modelAndView.addObject("trips", trips);
        }
        modelAndView.setViewName("driver_current_trip");
        return modelAndView;
    }

    @RequestMapping(value = "/arrive", method = RequestMethod.POST)
    ModelAndView requestTrip(@ModelAttribute(value = "currentTripId") Integer tripId){
        Trip trip = driverCurrentTripService.setState(tripId, "arrived");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("trip", trip);
        modelAndView.setViewName("driver_current_trip");
        return modelAndView;
    }
}
