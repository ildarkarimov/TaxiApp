package taxi.controllers.trip;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import taxi.pojo.DriverInfo;
import taxi.pojo.Trips;
import taxi.services.trip.DriverTripService;
@Controller
@SessionAttributes(value = "driverInfo")
public class DriverTripsController {
    @Autowired
    DriverTripService driverTripService;
    private static final Logger logger = Logger.getLogger(DriverTripsController.class);
    @RequestMapping(value = "/driverTrips", method = RequestMethod.POST)
    public ModelAndView showSetting(@ModelAttribute DriverInfo driverInfo){
        ModelAndView modelAndView = new ModelAndView();
        Trips driverTripsList = driverTripService.showDriverTrips(driverInfo.getId());
        modelAndView.addObject("driverTripsList", driverTripsList);
        modelAndView.setViewName("driver_trips");
        return modelAndView;
    }
}
