package taxi.controllers.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import taxi.pojo.ClientInfo;
import taxi.pojo.Trips;
import taxi.services.trip.ClientTripService;

@Controller
@SessionAttributes(value = "clientInfo")
public class ClientTripsController {
    @Autowired
    ClientTripService clientTripService;
    private static final Logger logger = Logger.getLogger(ClientTripsController.class);
    @RequestMapping(value = "/clientTrips", method = RequestMethod.POST)
    public ModelAndView showSetting(@ModelAttribute ClientInfo clientInfo){
        ModelAndView modelAndView = new ModelAndView();
        //ClientTripService clientTripService = new ClientTripServiceImpl();
        Trips clientTripsList = clientTripService.showClientTrips(clientInfo.getId());
        modelAndView.addObject("clientTripsList", clientTripsList);
        modelAndView.setViewName("client_trips");
        return modelAndView;
    }
}
