package taxi.controllers.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import taxi.database.dao.TripDAO;
import taxi.pojo.ClientInfo;
import taxi.pojo.Trip;
import taxi.services.trip.ClientCurrentTripService;
import taxi.services.trip.ClientCurrentTripServiceImpl;

@Controller
@SessionAttributes({"clientInfo", "tripId"})
public class ClientCurrentTripRequestController {
    @Autowired
    ClientCurrentTripService clientCurrentTripService;
    @Autowired
    TripDAO tripDAO;



    @RequestMapping (value = "/requestTrip", method = RequestMethod.GET)
    public ModelAndView showTripProgress(@ModelAttribute(value = "tripId") Integer tripId){
        ModelAndView modelAndView = new ModelAndView();
        Trip trip;
        if(tripId != 0) {
            trip = tripDAO.getTripById(tripId);
            modelAndView.addObject("trip", trip);
        }
        modelAndView.setViewName("client_current_trip");
        return modelAndView;
    }

    @RequestMapping(value = "/requestTrip", method = RequestMethod.POST)
    ModelAndView requestTrip(@ModelAttribute ClientInfo clientInfo,
                             @RequestParam(name = "startLocation", required = true) String startLocation,
                             @RequestParam(name = "endLocation", required = true) String endLocation,
                             @RequestParam(name = "paymentMethod", required = true) String paymentMethod){
        Trip trip = clientCurrentTripService.requestClientTrip(
                clientInfo.getId(), startLocation, endLocation, paymentMethod);
        Integer tripId = trip.getId();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("tripId", tripId);
        modelAndView.addObject("trip", trip);
        modelAndView.setViewName("client_current_trip");
        return modelAndView;
    }
}
