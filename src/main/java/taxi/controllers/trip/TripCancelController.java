package taxi.controllers.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import taxi.database.dao.TripDAO;
import taxi.pojo.Trip;
import taxi.services.trip.ClientCurrentTripService;
import taxi.services.trip.ClientCurrentTripServiceImpl;

@Controller
@SessionAttributes("tripId")
public class TripCancelController {
    @Autowired
    TripDAO tripDAO;
    @Autowired
    ClientCurrentTripService clientCurrentTripService;

    @RequestMapping (value = "/cancel", method = RequestMethod.GET)
    public ModelAndView showTripProgress(@ModelAttribute(value = "tripId") Integer tripId){
        ModelAndView modelAndView = new ModelAndView();
        Trip trip;
        if(tripId != 0) {
            trip = tripDAO.getTripById(tripId);
            modelAndView.addObject("trip", trip);
        }
        modelAndView.setViewName("client_current_trip");
        return modelAndView;
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    ModelAndView requestTrip(@ModelAttribute(value = "tripId") Integer tripId){
        Trip trip = clientCurrentTripService.cancelTrip(tripId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("trip", trip);
        modelAndView.setViewName("client_current_trip");
        return modelAndView;
    }
}
