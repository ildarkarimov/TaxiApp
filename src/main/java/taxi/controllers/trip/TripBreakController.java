package taxi.controllers.trip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import taxi.database.dao.TripDAO;
import taxi.pojo.Trip;
import taxi.pojo.Trips;
import taxi.services.trip.DriverCurrentTripService;

@Controller
@SessionAttributes(value = "currentTripId")
public class TripBreakController {
@Autowired
TripDAO tripDAO;


    @RequestMapping(value = "/breakTrip", method = RequestMethod.GET)
    public ModelAndView showTripProgress(@ModelAttribute(value = "currentTripId") Integer tripId){
        ModelAndView modelAndView = new ModelAndView();
        Trip trip;
        if(tripId != 0) {
            trip = tripDAO.getTripById(tripId);
            modelAndView.addObject("trip", trip);
        }
        else {
            Trips trips = tripDAO.getAllFreeTrips();
            modelAndView.addObject("trips", trips);
        }
        modelAndView.setViewName("driver_current_trip");
        return modelAndView;
    }


    @RequestMapping(value = "/breakTrip", method = RequestMethod.POST)
    ModelAndView breakTrip(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("currentTripId", 0);
        //Trip trip = tripDAO.getTripById(0);
        modelAndView.addObject("trip", null);
        modelAndView.setViewName("driver_current_trip");
        return modelAndView;
    }
}
