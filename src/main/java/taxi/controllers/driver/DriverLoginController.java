package taxi.controllers.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import taxi.pojo.Driver;
import taxi.pojo.DriverInfo;
import taxi.services.PasswordEncoder;
import taxi.services.driver.DriverAuthorizationService;
import taxi.services.driver.DriverAuthorizationServiceImpl;

@Controller
@SessionAttributes({"driverInfo", "isAuthDriver", "currentTripId"})
public class DriverLoginController {
    @Autowired
    DriverAuthorizationService as;
    DriverInfo driverInfo;
    @RequestMapping(value = "/authDriver", method = RequestMethod.POST)
    public ModelAndView showMenu(@RequestParam(name = "userName", required = true) String userName,
                                 @RequestParam(name = "password", required = true) String password){
        ModelAndView modelAndView = new ModelAndView();
        if ((driverInfo = as.authDriver(userName, PasswordEncoder.encode(password))) != null) {
            modelAndView.addObject("driverInfo", driverInfo);
            modelAndView.addObject("isAuthDriver", true);
            modelAndView.addObject("currentTripId", 0);
            modelAndView.setViewName("driver_menu");
        }
        else {
            modelAndView.addObject("answerDriver",
                    "Username or password incorrect. Try one more time");
            modelAndView.setViewName("index");
        }
        return modelAndView;
    }
}
