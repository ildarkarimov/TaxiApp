package taxi.controllers.driver;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DriverMenuController {
    @RequestMapping(value = "/driverMenu", method = RequestMethod.GET)
    public String showDriverMenu(){
        return "driver_menu";
    }
}
