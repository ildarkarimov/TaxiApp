package taxi.controllers.driver;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DriverLogoutController {
    @RequestMapping(value = "/driverLogout", method = RequestMethod.GET)
    public ModelAndView logout(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("isAuthDriver", false);
        modelAndView.setViewName("index");
        return modelAndView;
    }

}
