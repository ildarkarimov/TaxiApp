package taxi.controllers.driver;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import taxi.pojo.ClientInfo;
import taxi.pojo.DriverInfo;

@Controller
@SessionAttributes(value = "driverInfo")
public class DriverAccountSettingsController {
    @RequestMapping(value = "/driverSettings", method = RequestMethod.POST)
    public String showSetting(@ModelAttribute(value = "driverInfo") DriverInfo driverInfo){
        return "driver_settings";
    }
}
