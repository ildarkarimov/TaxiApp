package taxi.controllers.driver;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import taxi.controllers.client.ClientEditController;
import taxi.pojo.ClientInfo;
import taxi.pojo.DriverInfo;
import taxi.services.client.ClientEditService;
import taxi.services.driver.DriverEditService;

@Controller
@SessionAttributes(value = "driverInfo")
public class DriverEditController {
    @Autowired
    DriverEditService driverEditService;
    private static final Logger logger = Logger.getLogger(DriverEditController.class);
    @RequestMapping(value = "/driverEdit", method = RequestMethod.POST)
    public ModelAndView updateClient(@RequestParam(name = "id", required = true) int id,
                                     @RequestParam(name = "name", required = true) String name,
                                     @RequestParam(name = "cardNumber", required = true) int cardNumber,
                                     @RequestParam(name = "number", required = true) String number,
                                     @RequestParam(name = "make", required = true) String make,
                                     @RequestParam(name = "model", required = true) String model,
                                     @RequestParam(name = "colour", required = true) String colour,
                                     @ModelAttribute DriverInfo driverInfo){
        String answer = null;
        ModelAndView modelAndView = new ModelAndView();
        //ClientEditService clientEditService = new ClientEditServiceImpl();
        if(driverEditService.editDriver(id, name, cardNumber, number, make, model, colour)){
            driverInfo.setCardNumber(cardNumber);
            driverInfo.setName(name);
            driverInfo.setNumber(number);
            driverInfo.setModel(make);
            driverInfo.setModel(model);
            driverInfo.setColour(colour);
            answer = "Update is succeeded!";
        }
        modelAndView.addObject("answer", answer);
        modelAndView.setViewName("driver_settings");
        return modelAndView;
    }
}
