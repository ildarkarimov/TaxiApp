package taxi.controllers.driver;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import taxi.controllers.client.ClientEditPasswordController;
import taxi.pojo.ClientInfo;
import taxi.pojo.DriverInfo;
import taxi.services.client.ClientEditService;
import taxi.services.driver.DriverEditService;

@Controller
@SessionAttributes(value = "driverInfo")
public class DriverEditPasswordController {
    private static final Logger logger = Logger.getLogger(DriverEditPasswordController.class);
    @Autowired
    DriverEditService driverEditService;
    @RequestMapping(value = "/driverEditPassword", method = RequestMethod.POST)
    public ModelAndView changeDriverPassword(
            @ModelAttribute DriverInfo driverInfo,
            @RequestParam(name = "oldPassword", required = true) String oldPassword,
            @RequestParam(name = "newPassword", required = true) String newPassword){
        String answer = null;
        ModelAndView modelAndView = new ModelAndView();
        answer = driverEditService.editPassword(driverInfo.getId(), oldPassword, newPassword);
        modelAndView.addObject("answer", answer);
        modelAndView.addObject("oldPassword", oldPassword);
        modelAndView.addObject("newPassword", newPassword);
        modelAndView.setViewName("driver_edit_password");
        return modelAndView;
    }
    @RequestMapping (value = "/driverEditPassword", method = RequestMethod.GET)
    public String showPage(){
        return "driver_edit_password";
    }
}
