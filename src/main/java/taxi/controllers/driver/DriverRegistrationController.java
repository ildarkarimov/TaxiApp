package taxi.controllers.driver;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import taxi.controllers.client.ClientRegistrationController;
import taxi.services.client.ClientRegistrationService;
import taxi.services.driver.DriverRegistrationService;

@Controller
public class DriverRegistrationController {
    @Autowired
    DriverRegistrationService registrationService;
    private static final Logger logger = Logger.getLogger(DriverRegistrationController.class);
    @RequestMapping(value = "/driverRegistration", method = RequestMethod.GET)
    public String showPage(){
        return "driver_registration";
    }
    @RequestMapping(value = "/driverRegistration", method = RequestMethod.POST)
    public ModelAndView registrDriver(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "cardNumber") Integer cardNumber,
            @RequestParam(name = "userName") String userName,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "number") String number,
            @RequestParam(name = "make") String make,
            @RequestParam(name = "model") String model,
            @RequestParam(name = "colour") String colour){
        ModelAndView modelAndView = new ModelAndView();
        String answer = registrationService.regDriver(
                name, cardNumber, userName, password, number, make, model, colour);
        modelAndView.addObject("name", name);
        modelAndView.addObject("cardNumber", cardNumber);
        modelAndView.addObject("userName", userName);
        modelAndView.addObject("password", password);
        modelAndView.addObject("number", number);
        modelAndView.addObject("make", make);
        modelAndView.addObject("model", model);
        modelAndView.addObject("colour", colour);
        modelAndView.addObject("answer", answer);
        modelAndView.setViewName("driver_registration");
        return modelAndView;
    }
}
