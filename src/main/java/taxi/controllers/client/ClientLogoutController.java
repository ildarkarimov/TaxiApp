package taxi.controllers.client;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import taxi.pojo.ClientInfo;

@Controller
@SessionAttributes(value = "isAuthClient")
public class ClientLogoutController {
    @RequestMapping(value = "/clientLogout", method = RequestMethod.GET)
    public ModelAndView logout(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("isAuthClient", false);
        modelAndView.setViewName("index");
        return modelAndView;
    }

}
