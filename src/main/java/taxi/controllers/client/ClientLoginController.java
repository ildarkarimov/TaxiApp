package taxi.controllers.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import taxi.pojo.Client;
import taxi.pojo.ClientInfo;
import taxi.services.PasswordEncoder;
import taxi.services.client.ClientAuthorizationService;
import taxi.services.client.ClientAuthorizationServiceImpl;

@Controller
@SessionAttributes({"clientInfo", "isAuthClient", "tripId"})
public class ClientLoginController {
  @Autowired
  ClientAuthorizationService as;
  ClientInfo clientInfo;
    @RequestMapping(value = "/authClient", method = RequestMethod.POST)
    public ModelAndView showMenu(@RequestParam(name = "userName", required = true) String userName,
                                 @RequestParam(name = "password", required = true) String password){
        ModelAndView modelAndView = new ModelAndView();
        if ((clientInfo = as.authClient(userName, PasswordEncoder.encode(password))) != null) {
            modelAndView.addObject("clientInfo", clientInfo);
            modelAndView.addObject("isAuthClient", true);
            modelAndView.addObject("tripId", 0);
            modelAndView.setViewName("client_menu");
        }
        else {
            modelAndView.addObject("answerClient",
                    "Username or password incorrect. Try one more time");
            modelAndView.setViewName("index");
        }
        return modelAndView;
    }
}
