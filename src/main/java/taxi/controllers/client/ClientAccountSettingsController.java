package taxi.controllers.client;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import taxi.pojo.ClientInfo;

@Controller
@SessionAttributes(value = "clientInfo")
public class ClientAccountSettingsController {
    @RequestMapping(value = "/clientSettings", method = RequestMethod.POST)
    public String showSetting(@ModelAttribute (value = "clientInfo") ClientInfo clientInfo){
        return "client_settings";
    }
}
