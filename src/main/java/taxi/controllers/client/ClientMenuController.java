package taxi.controllers.client;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ClientMenuController {
    @RequestMapping(value = "/clientMenu", method = RequestMethod.GET)
    public String showClientMenu(){
        return "client_menu";
    }
}
