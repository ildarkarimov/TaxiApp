package taxi.controllers.client;

import org.springframework.beans.factory.annotation.Autowired;
import taxi.database.exceptions.ClientInsertException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import taxi.pojo.ClientInfo;
import taxi.services.client.ClientEditService;
import taxi.services.client.ClientEditServiceImpl;

@Controller
@SessionAttributes(value = "clientInfo")
public class ClientEditController {
    @Autowired
    ClientEditService clientEditService;
    private static final Logger logger = Logger.getLogger(ClientEditController.class);
    @RequestMapping(value = "/clientEdit", method = RequestMethod.POST)
    public ModelAndView updateClient(@RequestParam(name = "id", required = true) int id,
                                     @RequestParam(name = "name", required = true) String name,
                                     @RequestParam(name = "cardNumber", required = true) int cardNumber,
                                     @ModelAttribute ClientInfo clientInfo){
        String answer = null;
        ModelAndView modelAndView = new ModelAndView();
        //ClientEditService clientEditService = new ClientEditServiceImpl();
        if(clientEditService.editClient(id, name, cardNumber)){
            clientInfo.setCardNumber(cardNumber);
            clientInfo.setName(name);
            answer = "Update is succeeded!";
        }
        modelAndView.addObject("answer", answer);
        modelAndView.setViewName("client_settings");
        return modelAndView;
    }
}
