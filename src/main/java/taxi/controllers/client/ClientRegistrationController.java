package taxi.controllers.client;

import org.springframework.beans.factory.annotation.Autowired;
import taxi.database.exceptions.ClientGetException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import taxi.services.client.ClientRegistrationService;
import taxi.services.client.ClientRegistrationServiceImpl;

@Controller
public class ClientRegistrationController {
    @Autowired
    ClientRegistrationService registrationService;
    private static final Logger logger = Logger.getLogger(ClientRegistrationController.class);
    @RequestMapping(value = "/clientRegistration", method = RequestMethod.GET)
    public String showPage(){
        return "client_registration";
    }
    @RequestMapping(value = "/clientRegistration", method = RequestMethod.POST)
    public ModelAndView registrClient(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "cardNumber") Integer cardNumber,
            @RequestParam(name = "userName") String userName,
            @RequestParam(name = "password") String password){
        ModelAndView modelAndView = new ModelAndView();
        //ClientRegistrationService registrationService = new ClientRegistrationServiceImpl();
        String answer = registrationService.regClient(name, cardNumber, userName, password);
        modelAndView.addObject("name", name);
        modelAndView.addObject("cardNumber", cardNumber);
        modelAndView.addObject("userName", userName);
        modelAndView.addObject("password", password);
        modelAndView.addObject("answer", answer);
        modelAndView.setViewName("client_registration");
        return modelAndView;
    }
}
