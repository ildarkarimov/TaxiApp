package taxi.controllers.client;

import org.springframework.beans.factory.annotation.Autowired;
import taxi.database.exceptions.ClientInsertException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import taxi.pojo.ClientInfo;
import taxi.services.client.ClientEditService;
import taxi.services.client.ClientEditServiceImpl;

@Controller
@SessionAttributes(value = "clientInfo")
public class ClientEditPasswordController {
    private static final Logger logger = Logger.getLogger(ClientEditPasswordController.class);
    @Autowired
    ClientEditService clientEditService;
    @RequestMapping(value = "/clientEditPassword", method = RequestMethod.POST)
    public ModelAndView changeClientPassword(
            @ModelAttribute ClientInfo clientInfo,
            @RequestParam(name = "oldPassword", required = true) String oldPassword,
            @RequestParam(name = "newPassword", required = true) String newPassword){
        String answer = null;
        //ClientEditService clientEditService = new ClientEditServiceImpl();
        ModelAndView modelAndView = new ModelAndView();
            answer = clientEditService.editPassword(clientInfo.getId(), oldPassword, newPassword);
        modelAndView.addObject("answer", answer);
        modelAndView.addObject("oldPassword", oldPassword);
        modelAndView.addObject("newPassword", newPassword);
        modelAndView.setViewName("client_edit_password");
        return modelAndView;
    }
    @RequestMapping (value = "/clientEditPassword", method = RequestMethod.GET)
    public String showPage(){
        return "client_edit_password";
    }
}