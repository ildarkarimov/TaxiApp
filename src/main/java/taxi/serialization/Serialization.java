package taxi.serialization;

import taxi.pojo.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Serialization {
    public static void toXML(Object object, String fileName) {
        try {

            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(object, file);

            jaxbMarshaller.marshal(object, System.out);


        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /*public static void vehiclesToXML(Vehicles vehicles, String fileName) throws JAXBException {
        File file = new File(fileName);
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(Vehicles.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(vehicles, file);

        //jaxbMarshaller.marshal(vehicles, System.out);

    }*/
    public static void driversToXML(Drivers drivers, String fileName) throws JAXBException {
        File file = new File(fileName);
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(Drivers.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(drivers, file);

       // jaxbMarshaller.marshal(drivers, System.out);

    }
    public static void clientsToXML(Clients clients, String fileName) throws JAXBException {
        File file = new File(fileName);
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(Clients.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(clients, file);

        //jaxbMarshaller.marshal(clients, System.out);

    }
    public static void tripsToXML(Trips trips, String fileName) throws JAXBException {
        File file = new File(fileName);
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(Trips.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(trips, file);

        //jaxbMarshaller.marshal(trips, System.out);

    }
    /*public static List<Vehicle> vehiclesFromXML(String fileName) {
        List<Vehicle> vehicleList = new ArrayList<>();
        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(Vehicles.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Vehicles vehicles = (Vehicles) unmarshaller.unmarshal(file);
            for(int i = 0; i < vehicles.getVehicles().size(); i++) {

                Vehicle vehicle = new Vehicle(vehicles.getVehicles().get(i).getId()
                        , vehicles.getVehicles().get(i).getNumber()
                        , vehicles.getVehicles().get(i).getMake()
                        , vehicles.getVehicles().get(i).getModel()
                        , vehicles.getVehicles().get(i).getColour());
                vehicleList.add(vehicle);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return vehicleList;
    }*/
    /*public static List<Driver> driversFromXML(String fileName) {
        List<Driver> driverList = new ArrayList<>();
        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(Drivers.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Drivers drivers = (Drivers) unmarshaller.unmarshal(file);
            for(int i = 0; i < drivers.getDrivers().size(); i++) {
            Driver driver = new Driver(drivers.getDrivers().get(i).getId()
                    , drivers.getDrivers().get(i).getName()
                    , drivers.getDrivers().get(i).getCardNumber()
                    , drivers.getDrivers().get(i).getVehicle()
                    , drivers.getDrivers().get(i).getUserName()
                    , drivers.getDrivers().get(i).getPassword());
            driverList.add(driver);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return driverList;
    }*/
    public static List<Client> clientsFromXML(String fileName) {
        List<Client> clientList = new ArrayList<>();
        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(Clients.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Clients clients = (Clients) unmarshaller.unmarshal(file);
            for(int i = 0; i < clients.getClients().size(); i++) {
                Client client = new Client(clients.getClients().get(i).getId()
                            , clients.getClients().get(i).getName()
                            , clients.getClients().get(i).getCardNumber()
                            , clients.getClients().get(i).getUserName()
                            , clients.getClients().get(i).getPassword());
                clientList.add(client);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return clientList;
    }
   /* public static List<Trip> tripsFromXML(String fileName) {
        List<Trip> tripList = new ArrayList<>();
        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(Trips.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Trips trips = (Trips) unmarshaller.unmarshal(file);
            for(int i = 0; i < trips.getTrips().size(); i++) {
                Trip trip = new Trip(trips.getTrips().get(i).getId()
                            , trips.getTrips().get(i).getClient()
                            , trips.getTrips().get(i).getDriver()
                            , trips.getTrips().get(i).getState()
                            , trips.getTrips().get(i).getCost()
                            , trips.getTrips().get(i).getPaymentMethod()
                            , trips.getTrips().get(i).getRideStartTime()
                            , trips.getTrips().get(i).getRideEndTime()
                            , trips.getTrips().get(i).getRideStartLocation()
                            , trips.getTrips().get(i).getRideEndLocation()
                            , trips.getTrips().get(i).getRideStatus());
                tripList.add(trip);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return tripList;
    }*/


}
