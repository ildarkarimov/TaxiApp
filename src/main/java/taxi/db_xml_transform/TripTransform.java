package taxi.db_xml_transform;

import taxi.database.dao.TripDAOImpl;
import taxi.database.exceptions.TripGetException;
import taxi.database.exceptions.TripInsertException;
import taxi.pojo.Trip;
import taxi.pojo.Trips;
import taxi.serialization.Serialization;

import javax.xml.bind.JAXBException;
import java.util.List;

public class TripTransform {
    /*public static void tripXMLToDB(String fileName) {
        List<Trip> trips = Serialization.tripsFromXML(fileName);
        try {
            new TripDAOImpl().insertTrips(trips);
        } catch (TripInsertException e) {
            e.printStackTrace();
        }
    }*/
    public static void tripDBToXML(String fileName){
        Trips trips = new Trips();
        try {
            trips = new TripDAOImpl().getAllTrips();
        } catch (TripGetException e) {
            e.printStackTrace();
        }
        try {
            Serialization.tripsToXML(trips, fileName);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
