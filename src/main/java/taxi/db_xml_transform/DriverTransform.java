package taxi.db_xml_transform;

import taxi.database.dao.DriverDAOImpl;
import taxi.database.exceptions.DriverGetException;
import taxi.database.exceptions.DriverInsertException;
import taxi.pojo.Driver;
import taxi.pojo.Drivers;
import taxi.serialization.Serialization;

import javax.xml.bind.JAXBException;
import java.util.List;

public class DriverTransform {
    /*public static void driverXMLToDB(String fileName) {
        List<Driver> drivers = Serialization.driversFromXML(fileName);
        try {
            new DriverDAOImpl().insertDrivers(drivers);
        } catch (DriverInsertException e) {
            e.printStackTrace();
        }
    }*/
    public static void driverDBToXML(String fileName){
        Drivers drivers = new Drivers();
        try {
            drivers =  new DriverDAOImpl().getAllDrivers();
        } catch (DriverGetException e) {
            e.printStackTrace();
        }
        try {
            Serialization.driversToXML(drivers, fileName);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
