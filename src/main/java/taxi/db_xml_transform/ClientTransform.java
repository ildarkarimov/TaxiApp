package taxi.db_xml_transform;


import taxi.database.dao.ClientDAOImpl;
import taxi.database.exceptions.ClientGetException;
import taxi.database.exceptions.ClientInsertException;
import taxi.pojo.Client;
import taxi.pojo.Clients;
import taxi.serialization.Serialization;

import javax.xml.bind.JAXBException;
import java.util.List;

public class ClientTransform {

    public static void clientXMLToDB(String fileName) {
        List<Client> clients = Serialization.clientsFromXML(fileName);
        try {
            new ClientDAOImpl().insertClients(clients);
        } catch (ClientInsertException e) {
            e.printStackTrace();
        }
    }
    public static void clientDBToXML(String fileName){
        Clients clients = new Clients();
        try {
            clients = new ClientDAOImpl().getAllClients();
        } catch (ClientGetException e) {
            e.printStackTrace();
        }
        try {
            Serialization.clientsToXML(clients, fileName);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
