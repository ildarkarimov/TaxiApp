package taxi.database.connections;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
@Component
public class DBPoolCacheImpl implements DBPoolCache {
    String url = "jdbc:mysql://localhost:3306/taxi";
    String user = "root";
    String password = "123";
    DataSource datasource;
    public DBPoolCacheImpl() throws ClassNotFoundException {
        PoolProperties p = new PoolProperties();
        p.setUrl(url);
        p.setDriverClassName("org.gjt.mm.mysql.Driver");
        p.setUsername(user);
        p.setPassword(password);
        p.setJmxEnabled(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(5);
        p.setMaxWait(100);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(100);
        p.setMaxIdle(100);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
                        "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        datasource = new DataSource();
        datasource.setPoolProperties(p);
    }


    public Connection getConnection() throws SQLException {
        return datasource.getConnection();
    }

    public void putConnection(Connection connection) throws SQLException {
        datasource.close();
    }
}
