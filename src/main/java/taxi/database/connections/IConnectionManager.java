package taxi.database.connections;

import java.sql.Connection;

public interface IConnectionManager {
    Connection getConnection();
}
