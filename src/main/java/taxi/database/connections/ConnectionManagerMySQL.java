package taxi.database.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManagerMySQL implements IConnectionManager {

    private static final ConnectionManagerMySQL INSTANCE = new ConnectionManagerMySQL();
    private Connection connection;

    private ConnectionManagerMySQL() {
        try {
            Class.forName("org.gjt.mm.mysql.Driver");
            connection =
                    DriverManager.getConnection(
                            "jdbc:mysql://localhost:3306/taxi",
                            "root",
                            "123");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static synchronized ConnectionManagerMySQL getInstance() {
        return INSTANCE;
    }

    public Connection getConnection() {
        return connection;
    }
}
