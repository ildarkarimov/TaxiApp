package taxi.database.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.connections.DBPoolCache;
import taxi.database.connections.DBPoolCacheImpl;
import taxi.database.exceptions.ClientGetException;
import taxi.database.exceptions.ClientInsertException;
import org.apache.log4j.Logger;
import taxi.pojo.Client;
import taxi.pojo.Clients;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Component
public class ClientDAOImpl implements ClientDAO {

    private static final Logger logger = Logger.getLogger(ClientDAOImpl.class);
    @Autowired
    DBPoolCacheImpl pool;

    //private taxi.database.connections.DBPoolCache pool;
    private Connection connection = null;
    private PreparedStatement statement = null;
    private ResultSet resultSet = null;

    public ClientDAOImpl() {
        try {
            pool = new DBPoolCacheImpl();
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }


    @Override
    public Boolean isUserNameExist(String login) throws ClientGetException {
        try {
            String query = "SELECT userName FROM Client";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next())
                if(resultSet.getString("userName").equals(login)) {
                resultSet.close();
                statement.close();
                connection.close();
                    return true;
                }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new ClientGetException();

        }
        return false;
    }
    @Override
    public Client getClientByUserName(String userName) throws ClientGetException {
        try {
            String query = "SELECT * FROM Client WHERE userName = '" + userName + "'";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);

            while (resultSet.next()) {
                Client client = new Client(resultSet.getInt("id")
                        , resultSet.getString("name")
                        , resultSet.getInt("cardNumber")
                        , resultSet.getString("userName")
                        , resultSet.getString("password"));
                resultSet.close();
                statement.close();
                connection.close();
                return client;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new ClientGetException();
        }
        return null;
    }
    @Override
    public Client getClientByNameAndPassword(String userName, String password) throws ClientGetException {
        Client client = null;
        try {
            String query = "SELECT * FROM Client WHERE userName = ? AND password = ?";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            //PreparedStatement statement = manager.getConnection().prepareStatement(query);
            statement.setString(1, userName);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            //ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                client = new Client(resultSet.getInt("id")
                        , resultSet.getString("name")
                        , resultSet.getInt("cardNumber")
                        , resultSet.getString("userName")
                        , resultSet.getString("password"));
                resultSet.close();
                statement.close();
                connection.close();
                return client;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new ClientGetException();
        }
        return null;
    }
    @Override
    public Client getClientById(int id) {
        try {
            String query = "SELECT * FROM Client WHERE id =" + id;
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);

            while (resultSet.next()) {
                Client client = new Client(resultSet.getInt("id")
                , resultSet.getString("name")
                , resultSet.getInt("cardNumber")
                , resultSet.getString("userName")
                , resultSet.getString("password"));
                resultSet.close();
                statement.close();
                connection.close();
                return client;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new ClientGetException();
            } catch (ClientGetException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }
    @Override
    public Clients getAllClients() throws ClientGetException {
        Clients clients = new Clients();
        clients.setClients(new ArrayList<Client>());
        try {
            String query = "SELECT * FROM Client";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                Client client = new Client(resultSet.getInt("id")
                        , resultSet.getString("name")
                        , resultSet.getInt("cardNumber")
                        , resultSet.getString("userName")
                        , resultSet.getString("pw"));
                clients.getClients().add(client);
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new ClientGetException();
        }
        return clients;
    }
    @Override
    public void insertClients(List<Client> clientList) throws ClientInsertException {
        try {
            String query = "INSERT INTO Client (id, name, cardNumber, userName, pw) VALUES (?, ?, ?, ?, ?)";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            for(Client client: clientList) {
                statement.setInt(1, client.getId());
                statement.setString(2, client.getName());
                statement.setInt(3, client.getCardNumber());
                statement.setString(4, client.getUserName());
                statement.setString(5, client.getPassword());
                statement.addBatch();
            }
            statement.executeBatch();
            pool.putConnection(connection);
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new ClientInsertException();
        }
    }
    @Override
    public boolean insertClient(Client client) throws ClientInsertException {
        try {
            String query = "INSERT INTO Client (name, cardNumber, userName, password) VALUES (?, ?, ?, ?)";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
                //preparedStatement.setInt(1, client.getId());
                statement.setString(1, client.getName());
                statement.setInt(2, client.getCardNumber());
                statement.setString(3, client.getUserName());
                statement.setString(4, client.getPassword());
                if((statement.executeUpdate())==1) {
                    pool.putConnection(connection);
                    statement.close();
                    connection.close();
                    return true;
                }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new ClientInsertException();
        }
        return false;
    }
    @Override
    public boolean updateClient(String name, int cardNumber, int id) throws ClientInsertException {
            try {

                String query = "UPDATE Client SET name = ?, cardNumber = ? WHERE id = ?";
                connection = pool.getConnection();
                statement = connection.prepareStatement(query);
                statement.setString(1, name);
                statement.setInt(2, cardNumber);
                statement.setInt(3, id);
                if(statement.executeUpdate()==1) {
                    pool.putConnection(connection);
                    statement.close();
                    connection.close();
                    return true;
                }
            } catch (SQLException e) {
                logger.error(e.getMessage());
                throw new ClientInsertException();
            }
            return false;
    }
    @Override
    public boolean updatePassword(int id, String password) throws ClientInsertException {
        try {
            String query = "UPDATE Client SET password = ? WHERE id = ?";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, password);
            statement.setInt(2, id);
            if(statement.executeUpdate()==1) {
                pool.putConnection(connection);
                statement.close();
                connection.close();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new ClientInsertException();
        }
        return false;
    }
}
