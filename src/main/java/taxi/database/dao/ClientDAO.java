package taxi.database.dao;

import org.springframework.stereotype.Component;
import taxi.database.exceptions.ClientGetException;
import taxi.database.exceptions.ClientInsertException;
import taxi.pojo.Client;
import taxi.pojo.Clients;

import java.util.List;
@Component
public interface ClientDAO {
    public Client getClientById(int id);
    public Clients getAllClients() throws ClientGetException;
    public void insertClients(List<Client> clientList) throws ClientInsertException;
    public boolean insertClient(Client client) throws ClientInsertException;
    public boolean updateClient(String name, int cardNumber, int id) throws ClientInsertException;
    public Client getClientByUserName(String userName) throws ClientGetException;
    public Boolean isUserNameExist(String login) throws ClientGetException;
    public Client getClientByNameAndPassword(String userName, String password) throws ClientGetException;
    public boolean updatePassword(int id, String password) throws ClientInsertException;

}
