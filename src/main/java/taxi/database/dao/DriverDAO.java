package taxi.database.dao;

import org.springframework.stereotype.Component;
import taxi.database.exceptions.DriverGetException;
import taxi.database.exceptions.DriverInsertException;
import taxi.pojo.Client;
import taxi.pojo.Driver;
import taxi.pojo.Drivers;

import java.util.List;
@Component
public interface DriverDAO {
    public Driver getDriverByNameAndPassword(String userName, String password);
    public Driver getDriverById(int id);
    public Drivers getAllDrivers() throws DriverGetException;
    public Boolean isUserNameExist(String login);
    public boolean insertDriver(Driver driver);
    public void insertDrivers(List<Driver> driverList) throws DriverInsertException;
    public boolean updateDriver(String name, int cardNumber, String number, String make,
                                String model, String colour, int id);
    public boolean updatePassword(int id, String password);
}
