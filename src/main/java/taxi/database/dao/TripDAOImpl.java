package taxi.database.dao;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.connections.DBPoolCacheImpl;
import taxi.database.exceptions.ClientGetException;
import taxi.database.exceptions.TripGetException;
import taxi.database.exceptions.TripInsertException;
import org.apache.log4j.Logger;
import taxi.pojo.Client;
import taxi.pojo.Trip;
import taxi.pojo.Trips;
import taxi.pojo.Driver;
import taxi.pojo.state.InitState;
import taxi.pojo.state.State;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
@Component
public class TripDAOImpl  implements TripDAO{
    private static final Logger logger = Logger.getLogger(TripDAOImpl.class);
    @Autowired
    DBPoolCacheImpl pool;
    Connection connection;
    PreparedStatement statement;
    ResultSet resultSet;

    public TripDAOImpl() {
        try {
            pool = new DBPoolCacheImpl();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Integer getTripsCount(){
        String query = "SELECT COUNT(*) AS COUNT FROM Trip";
        try {
            Integer count = null;
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                count = resultSet.getInt("COUNT");
            }
            resultSet.close();
            statement.close();
            connection.close();
            return count;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }


    @Override
    public Trips getAllTrips() throws TripGetException {
        Trips trips = new Trips();
        trips.setTrips(new ArrayList<Trip>());
        try {
            String query = "SELECT * FROM Trip";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                Driver driver = null;
                Client client = null;
                ClientDAO clientDAO = new ClientDAOImpl();
                DriverDAO driverDAO = new DriverDAOImpl();
                try {
                    client = clientDAO.getClientById(resultSet.getInt("clientId"));
                } catch (ClientGetException e) {
                    logger.error(e.getMessage());
                }
                    driver = driverDAO.getDriverById(resultSet.getInt("driverId"));

                Trip trip = new Trip();
                trip.setId(resultSet.getInt("id"));
                trip.setClient(client);
                trip.setDriver(driver);
                trip.setPaymentMethod(resultSet.getString("paymentMethod"));
                State initState = new InitState(trip);
                initState.init(resultSet.getString("state"));
                trip.setState(initState);
                trip.setCost(resultSet.getDouble("cost"));
                if(resultSet.getDate("rideStartTime") != null)
                    trip.setRideStartTime(resultSet.getDate("rideStartTime").toString());
                if(resultSet.getDate("rideEndTime") != null)
                    trip.setRideEndTime(resultSet.getDate("rideEndTime").toString());
                trip.setRideStartLocation(resultSet.getString("rideStartLocation"));
                trip.setRideEndLocation(resultSet.getString("rideEndLocation"));
                trips.getTrips().add(trip);
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new TripGetException();
        }
        return trips;
    }

    @Override
    public Trips getAllFreeTrips(){
        Trips trips = new Trips();
        trips.setTrips(new ArrayList<Trip>());
        try {
            String query = "SELECT * FROM Trip WHERE driverId IS NULL AND state <> 'cancelled'";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                Driver driver = null;
                Client client = null;
                ClientDAO clientDAO = new ClientDAOImpl();
                DriverDAO driverDAO = new DriverDAOImpl();
                try {
                    client = clientDAO.getClientById(resultSet.getInt("clientId"));
                } catch (ClientGetException e) {
                    logger.error(e.getMessage());
                }
                driver = driverDAO.getDriverById(resultSet.getInt("driverId"));

                Trip trip = new Trip();
                trip.setId(resultSet.getInt("id"));
                trip.setClient(client);
                trip.setDriver(driver);
                trip.setPaymentMethod(resultSet.getString("paymentMethod"));
                State initState = new InitState(trip);
                initState.init(resultSet.getString("state"));
                trip.setState(initState);
                trip.setCost(resultSet.getDouble("cost"));
                if(resultSet.getDate("rideStartTime") != null)
                    trip.setRideStartTime(resultSet.getDate("rideStartTime").toString());
                if(resultSet.getDate("rideEndTime") != null)
                    trip.setRideEndTime(resultSet.getDate("rideEndTime").toString());
                trip.setRideStartLocation(resultSet.getString("rideStartLocation"));
                trip.setRideEndLocation(resultSet.getString("rideEndLocation"));
                trips.getTrips().add(trip);
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new TripGetException();
            } catch (TripGetException e1) {
                e1.printStackTrace();
            }
        }
        return trips;
    }


    @Override
    public Trips getClientTrips(int clientId){
        Trips clientTrips = new Trips();
        clientTrips.setTrips(new ArrayList<Trip>());
        try {
            String query = "SELECT * FROM Trip WHERE clientId = " + clientId;
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                Driver driver = null;
                Client client = null;
                ClientDAO clientDAO = new ClientDAOImpl();
                DriverDAO driverDAO = new DriverDAOImpl();
                client = clientDAO.getClientById(resultSet.getInt("clientId"));
                    driver = driverDAO.getDriverById(resultSet.getInt("driverId"));
                Trip trip = new Trip();
                trip.setId(resultSet.getInt("id"));
                trip.setClient(client);
                trip.setDriver(driver);
                trip.setPaymentMethod(resultSet.getString("paymentMethod"));
                State initState = new InitState(trip);
                initState.init(resultSet.getString("state"));
                trip.setState(initState);
                trip.setCost(resultSet.getDouble("cost"));
                if(resultSet.getDate("rideStartTime") != null)
                    trip.setRideStartTime(resultSet.getDate("rideStartTime").toString());
                if(resultSet.getDate("rideEndTime") != null)
                    trip.setRideEndTime(resultSet.getDate("rideEndTime").toString());
                trip.setRideStartLocation(resultSet.getString("rideStartLocation"));
                trip.setRideEndLocation(resultSet.getString("rideEndLocation"));
                clientTrips.getTrips().add(trip);
                resultSet.close();
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new TripGetException();
            } catch (TripGetException e1) {
                logger.error(e1.getMessage());
            }
        }
        return clientTrips;
    }

    @Override
    public Trips getDriverTrips(int driverId){
        Trips driverTrips = new Trips();
        driverTrips.setTrips(new ArrayList<Trip>());
        try {
            String query = "SELECT * FROM Trip WHERE driverId = " + driverId;
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                Driver driver = null;
                Client client = null;
                ClientDAO clientDAO = new ClientDAOImpl();
                DriverDAO driverDAO = new DriverDAOImpl();
                client = clientDAO.getClientById(resultSet.getInt("clientId"));
                driver = driverDAO.getDriverById(resultSet.getInt("driverId"));
                Trip trip = new Trip();
                trip.setId(resultSet.getInt("id"));
                trip.setClient(client);
                trip.setDriver(driver);
                trip.setPaymentMethod(resultSet.getString("paymentMethod"));
                State initState = new InitState(trip);
                initState.init(resultSet.getString("state"));
                trip.setState(initState);
                trip.setCost(resultSet.getDouble("cost"));
                if(resultSet.getDate("rideStartTime") != null)
                    trip.setRideStartTime(resultSet.getDate("rideStartTime").toString());
                if(resultSet.getDate("rideEndTime") != null)
                    trip.setRideEndTime(resultSet.getDate("rideEndTime").toString());
                trip.setRideStartLocation(resultSet.getString("rideStartLocation"));
                trip.setRideEndLocation(resultSet.getString("rideEndLocation"));
                driverTrips.getTrips().add(trip);
                resultSet.close();
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new TripGetException();
            } catch (TripGetException e1) {
                e1.printStackTrace();
            }
        }
        return driverTrips;
    }
    @Override
    public void insertTrips(List<Trip> tripList) throws TripInsertException {
        try {
            String query = "INSERT INTO Trip (id, clientId, driverId, paymentMethod, paymentStatus, cost" +
                    ", rideStartTime, rideEndTime, rideStartLocation, rideEndLocation, rideStatus)" +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            for(Trip trip: tripList) {
                statement.setInt(1, trip.getId());
                statement.setInt(2, trip.getClient().getId());
                statement.setInt(3, trip.getDriver().getId());
                statement.setString(4, trip.getPaymentMethod());
                statement.setString(5, trip.getState().getStateName());
                statement.setDouble(6, trip.getCost());
                statement.setDate(7, Date.valueOf(trip.getRideStartTime()));
                statement.setDate(8, Date.valueOf(trip.getRideEndTime()));
                statement.setString(9, trip.getRideStartLocation());
                statement.setString(10, trip.getRideEndLocation());
                statement.setString(11, trip.getRideStatus());
                statement.addBatch();
            }
            statement.executeBatch();
            pool.putConnection(connection);
            statement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new TripInsertException();
        }
    }
    @Override
    public void insertTrip(Trip trip){
        try {
            String query = "INSERT INTO Trip (id, clientId, driverId, paymentMethod, paymentStatus, cost" +
                    ", rideStartTime, rideEndTime, rideStartLocation, rideEndLocation)" +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
                statement.setInt(1, trip.getId());
                statement.setInt(2, trip.getClient().getId());
                if(trip.getDriver()!=null)
                statement.setInt(3, trip.getDriver().getId());
                statement.setString(4, trip.getPaymentMethod());
                statement.setString(5, trip.getState().getStateName());
                statement.setDouble(6, trip.getCost());
                statement.setDate(7, Date.valueOf(trip.getRideStartTime()));
                statement.setDate(8, Date.valueOf(trip.getRideEndTime()));
                statement.setString(9, trip.getRideStartLocation());
                statement.setString(10, trip.getRideEndLocation());
            if(statement.executeUpdate()==1) {
                pool.putConnection(connection);
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new TripInsertException();
            } catch (TripInsertException e1) {
                e1.printStackTrace();
            }
        }

    }
    @Override
    public void insertTripRequest(Trip trip){
        try {
            String query = "INSERT INTO Trip (id, clientId, state, " +
                    "rideStartLocation, rideEndLocation, paymentMethod) VALUES (?, ?, ?, ?, ?,?)";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, trip.getId());
            statement.setInt(2, trip.getClient().getId());
            statement.setString(3, trip.getState().getStateName());
            statement.setString(4, trip.getRideStartLocation());
            statement.setString(5, trip.getRideEndLocation());
            statement.setString(6, trip.getPaymentMethod());
            if(statement.executeUpdate()==1) {
                pool.putConnection(connection);
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new TripInsertException();
            } catch (TripInsertException e1) {
                e1.printStackTrace();
            }
        }

    }
    @Override
    public Trip getTripById(int id){
        try {
            Trip trip = new Trip();
            String query = "SELECT * FROM Trip WHERE id = " + id;
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                Driver driver = null;
                Client client = null;
                ClientDAO clientDAO = new ClientDAOImpl();
                DriverDAO driverDAO = new DriverDAOImpl();
                client = clientDAO.getClientById(resultSet.getInt("clientId"));
                driver = driverDAO.getDriverById(resultSet.getInt("driverId"));
                trip.setId(resultSet.getInt("id"));
                trip.setClient(client);
                trip.setDriver(driver);
                trip.setPaymentMethod(resultSet.getString("paymentMethod"));
                State initState = new InitState(trip);
                initState.init(resultSet.getString("state"));
                trip.setState(initState);
                trip.setCost(resultSet.getDouble("cost"));
                trip.setRideStartTime(null);
                if(resultSet.getDate("rideStartTime") != null)
                    trip.setRideStartTime(resultSet.getDate("rideStartTime").toString());
                if(resultSet.getDate("rideEndTime") != null)
                    trip.setRideEndTime(resultSet.getDate("rideEndTime").toString());
                trip.setRideStartLocation(resultSet.getString("rideStartLocation"));
                trip.setRideEndLocation(resultSet.getString("rideEndLocation"));
                resultSet.close();
                statement.close();
                connection.close();
                return trip;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new TripGetException();
            } catch (TripGetException e1) {
                logger.error(e1.getMessage());
            }
        }
        return null;
    }


    @Override
    public void updateTripAccept(Trip trip){
        try {
            String query = "UPDATE Trip SET driverId = ?, cost = ?, " +
                    "rideStartTime = ?, rideEndTime = ?,  state = ? WHERE id = ?";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, trip.getDriver().getId());
            statement.setDouble(2, trip.getCost());
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            statement.setDate(3, date);
            statement.setDate(4, date);
            statement.setString(5, trip.getState().getStateName());
            statement.setInt(6, trip.getId());
            if(statement.executeUpdate()==1) {
                pool.putConnection(connection);
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new TripInsertException();
            } catch (TripInsertException e1) {
                e1.printStackTrace();
            }
        }

    }

    @Override
    public void setState(int id, String state){
        try {
            String query = "UPDATE Trip SET state = ? WHERE id = ?";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, state);
            statement.setInt(2, id);
            if(statement.executeUpdate()==1) {
                pool.putConnection(connection);
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new TripInsertException();
            } catch (TripInsertException e1) {
                e1.printStackTrace();
            }
        }
    }
}
