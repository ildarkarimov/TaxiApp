package taxi.database.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxi.database.connections.DBPoolCacheImpl;
import taxi.database.exceptions.ClientInsertException;
import taxi.database.exceptions.DriverGetException;
import taxi.database.exceptions.DriverInsertException;
import taxi.database.exceptions.VehicleGetException;
import org.apache.log4j.Logger;
import taxi.pojo.Driver;
import taxi.pojo.Drivers;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Component
public class DriverDAOImpl implements DriverDAO {
    private static final Logger logger = Logger.getLogger(DriverDAOImpl.class);
    @Autowired
    DBPoolCacheImpl pool;
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet resultSet = null;


    public DriverDAOImpl() {
        try {
            pool = new DBPoolCacheImpl();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Boolean isUserNameExist(String login) {
        try {
            String query = "SELECT userName FROM Driver";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next())
                if(resultSet.getString("userName").equals(login)) {
                    resultSet.close();
                    statement.close();
                    connection.close();
                    return true;
                }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new DriverGetException();
            } catch (DriverGetException e1) {
                e1.printStackTrace();
            }

        }
        return false;
    }

    @Override
    public Driver getDriverByNameAndPassword(String userName, String password) {
        try {
            String query = "SELECT * FROM Driver WHERE userName = ? AND password = ?";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, userName);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                Driver driver = new Driver(
                        resultSet.getInt("id")
                        , resultSet.getString("name")
                        , resultSet.getInt("cardNumber")
                        , resultSet.getString("userName")
                        , resultSet.getString("password")
                        , resultSet.getString("number")
                        , resultSet.getString("make")
                        , resultSet.getString("model")
                        , resultSet.getString("colour"));
                resultSet.close();
                statement.close();
                connection.close();
                return driver;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new DriverGetException();
            } catch (DriverGetException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }
    @Override
    public Driver getDriverById(int id){
        try {
            String query = "SELECT * FROM Driver WHERE id = " + id;
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            pool.putConnection(connection);
            while (resultSet.next()) {
                Driver driver = new Driver(
                        resultSet.getInt("id")
                        , resultSet.getString("name")
                        , resultSet.getInt("cardNumber")
                        , resultSet.getString("userName")
                        , resultSet.getString("password")
                        , resultSet.getString("number")
                        , resultSet.getString("make")
                        , resultSet.getString("model")
                        , resultSet.getString("colour"));
                resultSet.close();
                statement.close();
                connection.close();
                return driver;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new DriverGetException();
            } catch (DriverGetException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }
    @Override
    public Drivers getAllDrivers() throws DriverGetException {
        Drivers drivers = new Drivers();
        drivers.setDrivers(new ArrayList<Driver>());
        try {
            String query = "SELECT * FROM Driver";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Driver driver = new Driver(
                        resultSet.getInt("id")
                        , resultSet.getString("name")
                        , resultSet.getInt("cardNumber")
                        , resultSet.getString("userName")
                        , resultSet.getString("pw")
                        , resultSet.getString("number")
                        , resultSet.getString("make")
                        , resultSet.getString("model")
                        , resultSet.getString("colour"));
                drivers.getDrivers().add(driver);
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DriverGetException();
        }
        return drivers;
    }

    @Override
    public boolean insertDriver(Driver driver){
        try {
            String query = "INSERT INTO Driver (" +
                    "name, cardNumber, userName, password, number, make, model, colour)" +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
                statement.setString(1, driver.getName());
                statement.setInt(2, driver.getCardNumber());
                statement.setString(3, driver.getUserName());
                statement.setString(4, driver.getPassword());
                statement.setString(5, driver.getNumber());
                statement.setString(6, driver.getMake());
                statement.setString(7, driver.getModel());
                statement.setString(8, driver.getColour());
            if(statement.executeUpdate()==1){
                pool.putConnection(connection);
                statement.close();
                connection.close();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new DriverInsertException();
            } catch (DriverInsertException e1) {
                e1.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void insertDrivers(List<Driver> driverList) throws DriverInsertException {
        try {
            String query = "INSERT INTO Driver (id, name, cardNumber, vehicleId, userName, pw)" +
                    " VALUES (?, ?, ?, ?, ?, ?)";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            for(Driver driver: driverList) {
                statement.setInt(1, driver.getId());
                statement.setString(2, driver.getName());
                statement.setInt(3, driver.getCardNumber());
                statement.setString(5, driver.getUserName());
                statement.setString(6, driver.getPassword());
                statement.addBatch();
            }
            statement.executeBatch();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DriverInsertException();
        }
    }

    @Override
    public boolean updateDriver(String name, int cardNumber, String number, String make,
                                String model, String colour, int id){
        try {

            String query = "UPDATE Driver SET name = ?, cardNumber = ?, number = ?, make = ?," +
                    " model = ?, colour = ? WHERE id = ?";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            statement.setInt(2, cardNumber);
            statement.setString(3, number);
            statement.setString(4, make);
            statement.setString(5, model);
            statement.setString(6, colour);
            statement.setInt(7, id);
            if(statement.executeUpdate()==1) {
                pool.putConnection(connection);
                statement.close();
                connection.close();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new DriverInsertException();
            } catch (DriverInsertException e1) {
                e1.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean updatePassword(int id, String password) {
        try {
            String query = "UPDATE Driver SET password = ? WHERE id = ?";
            connection = pool.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, password);
            statement.setInt(2, id);
            if(statement.executeUpdate()==1) {
                pool.putConnection(connection);
                statement.close();
                connection.close();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                throw new DriverInsertException();
            } catch (DriverInsertException e1) {
                e1.printStackTrace();
            }
        }
        return false;
    }
}
