package taxi.database.dao;

import org.springframework.stereotype.Component;
import taxi.database.exceptions.TripGetException;
import taxi.database.exceptions.TripInsertException;
import taxi.pojo.Trip;
import taxi.pojo.Trips;

import java.util.List;
@Component
public interface TripDAO {
    public Trips getAllTrips() throws TripGetException;
    public Trips getClientTrips(int clientId);
    public Trips getDriverTrips(int driverId);
    public Trip getTripById(int id);
    public void insertTrips(List<Trip> tripList) throws TripInsertException;
    public void insertTrip(Trip trip);
    public Integer getTripsCount();
    public void insertTripRequest(Trip trip);
    public Trips getAllFreeTrips();
    public void setState(int id, String state);
    public void updateTripAccept(Trip trip);
}
