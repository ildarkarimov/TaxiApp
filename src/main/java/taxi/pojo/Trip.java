
package taxi.pojo;

import org.springframework.stereotype.Component;
import taxi.pojo.state.State;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Trip complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Trip">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="client" type="{}Client"/>
 *         &lt;element name="driver" type="{}Driver"/>
 *         &lt;element name="paymentMethod" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cost" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="rideStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="rideEndTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="rideStartLocation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rideEndLocation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rideStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Trip", propOrder = {
    "id",
    "client",
    "driver",
    "paymentMethod",
        "state",
    "cost",
    "rideStartTime",
    "rideEndTime",
    "rideStartLocation",
    "rideEndLocation",
    "rideStatus"
})
@Component
public class Trip {

    protected int id;
    @XmlElement(required = true)
    protected Client client;
    @XmlElement(required = true)
    protected Driver driver;
    protected String paymentMethod;
    protected State state;
    protected double cost;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected String rideStartTime;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected String rideEndTime;
    @XmlElement(required = true)
    protected String rideStartLocation;
    @XmlElement(required = true)
    protected String rideEndLocation;
    @XmlElement(required = true)
    protected String rideStatus;

    public Trip() {
    }

    public Trip(int id, Client client, Driver driver, String paymentMethod, State state, double cost,
                String rideStartTime, String rideEndTime, String rideStartLocation,
                String rideEndLocation, String rideStatus) {
        this.id = id;
        this.client = client;
        this.driver = driver;
        this.paymentMethod = paymentMethod;
        this.state = state;
        this.cost = cost;
        this.rideStartTime = rideStartTime;
        this.rideEndTime = rideEndTime;
        this.rideStartLocation = rideStartLocation;
        this.rideEndLocation = rideEndLocation;
        this.rideStatus = rideStatus;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", client=" + client +
                ", driver=" + driver +
                ", paymentMethod=" + paymentMethod +
                ", state=" + state +
                ", cost=" + cost +
                ", rideStartTime='" + rideStartTime + '\'' +
                ", rideEndTime='" + rideEndTime + '\'' +
                ", rideStartLocation='" + rideStartLocation + '\'' +
                ", rideEndLocation='" + rideEndLocation + '\'' +
                ", rideStatus='" + rideStatus + '\'' +
                '}';
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the client property.
     * 
     * @return
     *     possible object is
     *     {@link Client }
     *     
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value
     *     allowed object is
     *     {@link Client }
     *     
     */
    public void setClient(Client value) {
        this.client = value;
    }

    /**
     * Gets the value of the driver property.
     * 
     * @return
     *     possible object is
     *     {@link Driver }
     *     
     */
    public Driver getDriver() {
        return driver;
    }

    /**
     * Sets the value of the driver property.
     * 
     * @param value
     *     allowed object is
     *     {@link Driver }
     *     
     */
    public void setDriver(Driver value) {
        this.driver = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     *
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the state property.
     * 
     */
    public State getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     */
    public void setState(State value) {
        this.state = value;
    }

    /**
     * Gets the value of the cost property.
     * 
     */
    public double getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     */
    public void setCost(double value) {
        this.cost = value;
    }

    /**
     * Gets the value of the rideStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public String getRideStartTime() {
        return rideStartTime;
    }

    /**
     * Sets the value of the rideStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRideStartTime(String value) {
        this.rideStartTime = value;
    }

    /**
     * Gets the value of the rideEndTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public String getRideEndTime() {
        return rideEndTime;
    }

    /**
     * Sets the value of the rideEndTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRideEndTime(String value) {
        this.rideEndTime = value;
    }

    /**
     * Gets the value of the rideStartLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRideStartLocation() {
        return rideStartLocation;
    }

    /**
     * Sets the value of the rideStartLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRideStartLocation(String value) {
        this.rideStartLocation = value;
    }

    /**
     * Gets the value of the rideEndLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRideEndLocation() {
        return rideEndLocation;
    }

    /**
     * Sets the value of the rideEndLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRideEndLocation(String value) {
        this.rideEndLocation = value;
    }

    /**
     * Gets the value of the rideStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRideStatus() {
        return rideStatus;
    }

    /**
     * Sets the value of the rideStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRideStatus(String value) {
        this.rideStatus = value;
    }

}
