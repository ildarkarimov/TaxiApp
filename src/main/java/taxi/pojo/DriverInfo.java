package taxi.pojo;

import org.springframework.stereotype.Component;

@Component
public class DriverInfo {
    private int id;
    private String name;
    private int cardNumber;
    private String userName;
    private String number;
    private String make;
    private String model;
    private String colour;

    public DriverInfo() {
    }

    public DriverInfo(int id, String name, int cardNumber, String userName, String number, String make, String model, String colour) {
        this.id = id;
        this.name = name;
        this.cardNumber = cardNumber;
        this.userName = userName;
        this.number = number;
        this.make = make;
        this.model = model;
        this.colour = colour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }
}
