package taxi.pojo;

import org.springframework.stereotype.Component;

@Component
public class ClientInfo {
    protected int id;
    protected String name;
    protected int cardNumber;
    protected String userName;


    public ClientInfo() {
    }

    public ClientInfo(int id, String name, int cardNumber, String userName) {
        this.id = id;
        this.name = name;
        this.cardNumber = cardNumber;
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
