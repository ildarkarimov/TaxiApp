
package taxi.pojo;

public class Driver {

    protected int id;
    protected String name;
    protected int cardNumber;
    protected String userName;
    protected String password;
    protected String number;
    protected String make;
    protected String model;
    protected String colour;

    public Driver() {
    }

    public Driver(String name, int cardNumber, String userName, String password, String number, String make, String model, String colour) {
        this.name = name;
        this.cardNumber = cardNumber;
        this.userName = userName;
        this.password = password;
        this.number = number;
        this.make = make;
        this.model = model;
        this.colour = colour;
    }

    public Driver(int id, String name, int cardNumber, String userName, String password, String number, String make, String model, String colour) {
        this.id = id;
        this.name = name;
        this.cardNumber = cardNumber;
        this.userName = userName;
        this.password = password;
        this.number = number;
        this.make = make;
        this.model = model;
        this.colour = colour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cardNumber=" + cardNumber +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", number='" + number + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", colour='" + colour + '\'' +
                '}';
    }
}
