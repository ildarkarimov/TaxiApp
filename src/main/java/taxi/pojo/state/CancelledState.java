package taxi.pojo.state;

import taxi.pojo.Trip;

public class CancelledState extends State {
    public CancelledState(Trip trip) {
        super(trip);
        stateName = "cancelled";
    }

    @Override
    public void init(String state) {

    }

    @Override
    public void accept() {

    }

    @Override
    public void prepare() {

    }

    @Override
    public void start() {

    }

    @Override
    public void arrive() {

    }

    @Override
    public void pay() {

    }

    @Override
    public void cancel() {

    }
}
