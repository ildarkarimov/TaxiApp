package taxi.pojo.state;

import taxi.pojo.Trip;

public class StartedState extends State {
    public StartedState(Trip trip) {
        super(trip);
    }

    @Override
    public void init(String state) {

    }

    @Override
    public void accept() {

    }

    @Override
    public void prepare() {

    }

    @Override
    public void start() {

    }

    @Override
    public void arrive() {
        trip.setState(new ArrivedState(trip));
    }

    @Override
    public void pay() {

    }

    @Override
    public void cancel() {

    }
}
