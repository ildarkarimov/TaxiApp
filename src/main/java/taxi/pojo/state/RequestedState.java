package taxi.pojo.state;

import taxi.pojo.Trip;

public class RequestedState extends State {
    public RequestedState(Trip trip) {
        super(trip);
        stateName = "requested";
    }

    @Override
    public void init(String state) {

    }

    @Override
    public void accept() {
        trip.setState(new AcceptedState(trip));
    }

    @Override
    public void prepare() {

    }

    @Override
    public void start() {

    }

    @Override
    public void arrive() {

    }

    @Override
    public void pay() {

    }

    @Override
    public void cancel() {
        trip.setState(new CancelledState(trip));
    }
}
