package taxi.pojo.state;

import taxi.pojo.Trip;

public abstract class State {
    Trip trip;
    String stateName;

    public State(Trip trip) {
        this.trip = trip;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public abstract void init(String state);
    public abstract void accept();
    public abstract void prepare();
    public abstract void start();
    public abstract void arrive();
    public abstract void pay();
    public abstract void cancel();
}
