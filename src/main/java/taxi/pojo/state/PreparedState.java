package taxi.pojo.state;

import taxi.pojo.Trip;

public class PreparedState extends State {
    public PreparedState(Trip trip) {
        super(trip);
    }

    @Override
    public void init(String state) {

    }

    @Override
    public void accept() {

    }

    @Override
    public void prepare() {

    }

    @Override
    public void start() {
        trip.setState(new StartedState(trip));
    }

    @Override
    public void arrive() {

    }

    @Override
    public void pay() {

    }

    @Override
    public void cancel() {
        trip.setState(new CancelledState(trip));
    }
}
