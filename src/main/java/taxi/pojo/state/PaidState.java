package taxi.pojo.state;

import taxi.pojo.Trip;

public class PaidState extends State {
    public PaidState(Trip trip) {
        super(trip);
    }

    @Override
    public void init(String state) {

    }

    @Override
    public void accept() {

    }

    @Override
    public void prepare() {

    }

    @Override
    public void start() {

    }

    @Override
    public void arrive() {

    }

    @Override
    public void pay() {

    }

    @Override
    public void cancel() {

    }
}
