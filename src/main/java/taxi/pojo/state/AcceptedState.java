package taxi.pojo.state;

import taxi.pojo.Trip;

public class AcceptedState extends State {
    public AcceptedState(Trip trip) {
        super(trip);
        stateName = "accepted";
    }

    @Override
    public void init(String state) {

    }

    @Override
    public void accept() {

    }

    @Override
    public void prepare() {
        trip.setState(new PreparedState(trip));
    }

    @Override
    public void start() {

    }

    @Override
    public void arrive() {

    }

    @Override
    public void pay() {

    }

    @Override
    public void cancel() {
        trip.setState(new CancelledState(trip));
    }
}
