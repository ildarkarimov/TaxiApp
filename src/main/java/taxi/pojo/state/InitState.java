package taxi.pojo.state;

import taxi.pojo.Trip;

public class InitState extends State {
    public InitState(Trip trip) {
        super(trip);
    }

    @Override
    public void init(String state) {
        if(state.equals("requested")) {
            stateName = "requested";
            trip.setState(new RequestedState(trip));
        }
        if(state.equals("accepted")){
            stateName = "accepted";
            trip.setState(new AcceptedState(trip));
        }
        if(state.equals("prepared")){
            stateName = "prepared";
            trip.setState(new PreparedState(trip));
        }
        if(state.equals("started")){
            stateName = "started";
            trip.setState(new StartedState(trip));
        }
        if(state.equals("arrived")){
            stateName = "arrived";
            trip.setState(new ArrivedState(trip));
        }
        if(state.equals("paid")){
            stateName = "paid";
            trip.setState(new PaidState(trip));
        }
        if(state.equals("cancelled")){
            stateName = "cancelled";
            trip.setState(new CancelledState(trip));
        }


    }

    @Override
    public void accept() {

    }

    @Override
    public void prepare() {

    }

    @Override
    public void start() {

    }

    @Override
    public void arrive() {

    }

    @Override
    public void pay() {

    }

    @Override
    public void cancel() {

    }
}
